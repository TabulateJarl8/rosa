use core::ptr::NonNull;

use acpi::{AcpiHandler, PhysicalMapping};
use x86_64::{
    instructions::port::Port,
    structures::paging::{
        mapper::MapToError, FrameAllocator, Mapper, Page, PageSize, PageTableFlags, Size4KiB,
    },
    VirtAddr,
};

use crate::{acpi::PHYS_OFFSET, serial_println, FRAME_ALLOCATOR, MAPPER};

#[derive(Clone, Debug, Copy)]
pub struct KernelAcpi;

impl AcpiHandler for KernelAcpi {
    unsafe fn map_physical_region<T>(
        &self,
        physical_address: usize,
        size: usize,
    ) -> acpi::PhysicalMapping<Self, T> {
        let physical_mapping_page = Page::<Size4KiB>::containing_address(VirtAddr::new(
            (physical_address + *PHYS_OFFSET.get().unwrap() as usize) as u64,
        ));
        serial_println!(
            "addr: {:x?} {:x?}",
            physical_address,
            *PHYS_OFFSET.get().unwrap()
        );

        let page_size = physical_mapping_page.size() as usize;

        let page_padded = ((size - 1) / page_size + 1) * page_size;

        let virtual_start = physical_address + *PHYS_OFFSET.get().unwrap() as usize;
        let page_range = {
            let heap_start_page = Page::containing_address(VirtAddr::new(virtual_start as u64));
            let heap_end_page =
                Page::containing_address(VirtAddr::new((virtual_start + page_padded) as u64));
            Page::range_inclusive(heap_start_page, heap_end_page)
        };

        for page in page_range {
            let frame = {
                let mut locked_indicator = FRAME_ALLOCATOR.lock();
                let frame_allocator = locked_indicator.get_mut().unwrap();

                frame_allocator
                    .allocate_frame()
                    .ok_or(MapToError::<Size4KiB>::FrameAllocationFailed)
                    .expect("SOMETHING TERRIBLE HAS HAPPENED DURING ALLOCATION")
            };

            let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE;

            let mut locked_mapper = MAPPER.lock();
            let mut locked_allocator = FRAME_ALLOCATOR.lock();
            let frame_allocator = locked_allocator.get_mut().unwrap();

            let mapper = locked_mapper.get_mut().unwrap();
            serial_println!("{:?} {:?} {:?} {:?}", page, frame, page_range, page_padded);
            let res = unsafe { mapper.map_to(page, frame, flags, frame_allocator) };

            let flush = match res {
                Ok(val) => Some(val),
                Err(e) => match e {
                    MapToError::FrameAllocationFailed => panic!("out of memory"),
                    MapToError::ParentEntryHugePage => None,
                    MapToError::PageAlreadyMapped(_) => None,
                },
            };

            if let Some(flush) = flush {
                flush.flush();
            }
        }

        unsafe {
            PhysicalMapping::new(
                physical_address,
                NonNull::new(virtual_start as *mut _).unwrap(),
                size,
                page_padded,
                Self,
            )
        }
    }

    fn unmap_physical_region<T>(region: &acpi::PhysicalMapping<Self, T>) {
        let virtual_start = region.virtual_start().as_ptr() as u64;
        let size = region.region_length();

        let page_size = Size4KiB::SIZE as usize;
        // let page_count = (size + page_size - 1) / page_size;
        let page_count = size.div_ceil(page_size);

        let start_page = Page::<Size4KiB>::containing_address(VirtAddr::new(virtual_start));

        for i in 0..page_count {
            let page = start_page + i as u64;
            // let mut locked_mapper = MAPPER.lock();
            if let Some(mapper) = MAPPER.lock().get_mut() {
                if let Ok((_frame, flush)) = mapper.unmap(page) {
                    flush.flush();
                }
            }
        }

        serial_println!(
            "Unmapped region at virtual address 0x{:x} with size {}",
            virtual_start,
            size
        );
    }
}

#[derive(Debug, Clone, Copy)]
pub struct AmlHandler;

impl aml::Handler for AmlHandler {
    fn read_u8(&self, address: usize) -> u8 {
        unsafe { core::ptr::read_volatile(address as *const u8) }
    }

    fn read_u16(&self, address: usize) -> u16 {
        unsafe { core::ptr::read_volatile(address as *const u16) }
    }

    fn read_u32(&self, address: usize) -> u32 {
        unsafe { core::ptr::read_volatile(address as *const u32) }
    }

    fn read_u64(&self, address: usize) -> u64 {
        unsafe { core::ptr::read_volatile(address as *const u64) }
    }

    fn write_u8(&mut self, address: usize, value: u8) {
        unsafe { core::ptr::write_volatile(address as *mut u8, value) }
    }

    fn write_u16(&mut self, address: usize, value: u16) {
        unsafe { core::ptr::write_volatile(address as *mut u16, value) }
    }

    fn write_u32(&mut self, address: usize, value: u32) {
        unsafe { core::ptr::write_volatile(address as *mut u32, value) }
    }

    fn write_u64(&mut self, address: usize, value: u64) {
        unsafe { core::ptr::write_volatile(address as *mut u64, value) }
    }

    #[allow(unused_variables)]
    fn read_pci_u8(&self, segment: u16, bus: u8, device: u8, function: u8, offset: u16) -> u8 {
        todo!()
    }

    #[allow(unused_variables)]
    fn read_pci_u16(&self, segment: u16, bus: u8, device: u8, function: u8, offset: u16) -> u16 {
        todo!()
    }

    #[allow(unused_variables)]
    fn read_pci_u32(&self, segment: u16, bus: u8, device: u8, function: u8, offset: u16) -> u32 {
        todo!()
    }

    #[allow(unused_variables)]
    fn write_pci_u8(
        &self,
        segment: u16,
        bus: u8,
        device: u8,
        function: u8,
        offset: u16,
        value: u8,
    ) {
        todo!()
    }

    #[allow(unused_variables)]
    fn write_pci_u16(
        &self,
        segment: u16,
        bus: u8,
        device: u8,
        function: u8,
        offset: u16,
        value: u16,
    ) {
        todo!()
    }

    #[allow(unused_variables)]
    fn write_pci_u32(
        &self,
        segment: u16,
        bus: u8,
        device: u8,
        function: u8,
        offset: u16,
        value: u32,
    ) {
        todo!()
    }

    fn read_io_u8(&self, port: u16) -> u8 {
        unsafe { Port::new(port).read() }
    }

    fn read_io_u16(&self, port: u16) -> u16 {
        unsafe { Port::new(port).read() }
    }

    fn read_io_u32(&self, port: u16) -> u32 {
        unsafe { Port::new(port).read() }
    }

    fn write_io_u8(&self, port: u16, value: u8) {
        unsafe { Port::new(port).write(value) }
    }

    fn write_io_u16(&self, port: u16, value: u16) {
        unsafe { Port::new(port).write(value) }
    }

    fn write_io_u32(&self, port: u16, value: u32) {
        unsafe { Port::new(port).write(value) }
    }
}
