use conquer_once::spin::OnceCell;

pub mod handler;
pub mod mapping;
pub mod power;
pub mod tables;

pub static PHYS_OFFSET: OnceCell<u64> = OnceCell::uninit();
