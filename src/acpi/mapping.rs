use x86_64::{PhysAddr, VirtAddr};

use crate::{acpi::PHYS_OFFSET, serial_println};

pub fn phys_to_virt(offset: PhysAddr) -> VirtAddr {
    serial_println!("offset: {:?}", offset);
    VirtAddr::new(
        PHYS_OFFSET
            .get()
            .unwrap()
            .checked_add(offset.as_u64())
            .expect("phys to virt overflow"),
    )
}
