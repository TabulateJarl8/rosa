use aml::{AmlContext, AmlName, AmlValue};
use x86_64::instructions::port::Port;

use crate::{acpi::tables::get_fadt, println, serial_println};

use super::tables::get_dsdt;

struct PowerInfo {
    pm1a_control_block: u16,
    slp_typa: u8,
}

fn get_acpi_shutdown_info(dsdt_aml: &AmlContext) -> Option<PowerInfo> {
    let s5_object = dsdt_aml
        .namespace
        .get_by_path(&AmlName::from_str("\\_S5").ok()?)
        .ok()?;

    println!("Found _S5 object: {:?}", s5_object);

    // get the SLP typa and typb values
    let pkg = match s5_object {
        AmlValue::Package(pkg) => pkg,
        _ => return None,
    };

    let slp_typa = match pkg.get(1) {
        Some(AmlValue::Integer(val)) => *val as u8,
        _ => return None,
    };

    println!("Found slp_typa: {:?}", slp_typa);

    let pm1a_control_block: u16 = get_fadt()
        .ok()?
        .pm1a_control_block()
        .ok()?
        .address
        .try_into()
        .unwrap();

    println!("Found pm1a_control_block: {:?}", pm1a_control_block);

    Some(PowerInfo {
        pm1a_control_block,
        slp_typa,
    })
}

fn qemu_shutdown() {
    serial_println!("Trying qemu shutdown...");
    unsafe { Port::new(0x604).write(0x2000_u16) };
}

fn reset_8086() {
    serial_println!("Trying 8086 reset...");
    unsafe { Port::new(0x64).write(0xfe_u8) }
}

pub fn acpi_shutdown() -> () {
    println!("Fetching dsdt...");
    let dsdt = match get_dsdt() {
        Ok(val) => val,
        Err(e) => {
            println!("{:?}", e);
            return;
        }
    };
    println!("Got dsdt");
    if let Some(shutdown_info) = get_acpi_shutdown_info(&dsdt) {
        println!("Performing ACPI shutdown...");

        const SLP_EN: u16 = 1 << 13;
        let slp_cmd = ((shutdown_info.slp_typa as u16) << 10) | SLP_EN;

        println!("SLP cmd: {}", slp_cmd);

        unsafe { Port::new(shutdown_info.pm1a_control_block).write(slp_cmd) };

        println!("cmd written");

        // small delay to ensure command is processed
        for _ in 0..10000 {
            core::hint::spin_loop();
        }
    };

    serial_println!("ACPI, shutdown failed. Trying fallback methods...");

    // try shutting down a qemu VM
    qemu_shutdown();

    // try doing a keyboard controller reset
    reset_8086();

    serial_println!("All shutdown methods failed. Halting CPU...");
    crate::hlt_loop();
}
