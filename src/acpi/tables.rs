use acpi::{fadt::Fadt, AcpiHandler, AcpiTables, PhysicalMapping};
use alloc::boxed::Box;
use aml::{AmlContext, DebugVerbosity};
use x86_64::instructions::port::Port;

use crate::{acpi::handler::AmlHandler, println, serial_println};

use super::{handler::KernelAcpi, PHYS_OFFSET};

fn validate_phys_offset() -> Result<(), &'static str> {
    if PHYS_OFFSET.get().is_none() {
        return Err("PHYS_OFFSET is not initialized");
    }

    Ok(())
}

pub fn init_acpi(physical_memory_offset: u64) -> Result<(), &'static str> {
    PHYS_OFFSET.get_or_init(|| physical_memory_offset);

    let fadt = get_fadt()?;

    // the PM1a control block is one thing that tells us if the ACPI is enabled
    let pm1a_control_block_port: u16 = fadt
        .pm1a_control_block()
        .unwrap()
        .address
        .try_into()
        .unwrap();
    let pm1a_control_block_value = unsafe { Port::<u16>::new(pm1a_control_block_port).read() } & 1;

    // Check if ACPI is enabled
    if fadt.smi_cmd_port != 0
        || fadt.acpi_enable != 0
        || fadt.acpi_disable != 0
        || pm1a_control_block_value == 0
    {
        serial_println!("ACPI not enabled. Attempting to enable...");
        unsafe { Port::new(fadt.smi_cmd_port as u16).write(fadt.acpi_enable) };

        // poll the ACPI until it is ready
        while unsafe { Port::<u8>::new(pm1a_control_block_port).read() } & 1 == 0 {
            serial_println!("Polling the ACPI to start...");
        }
    }

    Ok(())
}

pub fn get_acpi_tables(acpi_handler: &KernelAcpi) -> Result<AcpiTables<KernelAcpi>, &'static str> {
    validate_phys_offset()?;

    unsafe {
        match AcpiTables::search_for_rsdp_bios(*acpi_handler) {
            Ok(tables) => {
                serial_println!("ACPI tables found successfully");
                Ok(tables)
            }
            Err(err) => {
                serial_println!("Failed to find ACPI tables: {:?}", err);
                Err("Failed to find ACPI tables")
            }
        }
    }
}

pub fn get_dsdt() -> Result<AmlContext, &'static str> {
    validate_phys_offset()?;

    println!("phys offset validated");

    let handler = KernelAcpi;

    unsafe {
        // Find the ACPI tables
        println!("finding tables...");
        let tables = get_acpi_tables(&handler)?;

        serial_println!("{:?}", tables);
        println!("{:?}", tables.platform_info());

        // Find the DSDT
        let dsdt = match tables.dsdt() {
            Ok(dsdt) => dsdt,
            Err(_) => return Err("No DSDT found in ACPI tables"),
        };

        println!(
            "DSDT found at physical address: 0x{:x}, length: {}",
            dsdt.address, dsdt.length
        );

        // Map the DSDT region into memory
        let dsdt_mapping = handler.map_physical_region::<u8>(
            dsdt.address as usize,
            usize::try_from(dsdt.length).unwrap_or(0x10000),
        );

        // Find the address and length of the DSDT mapping
        let stream_addr = dsdt_mapping.virtual_start().as_ptr();
        let stream_len = dsdt_mapping.region_length();

        println!(
            "DSDT virtual address: {:p}, length: {}",
            stream_addr, stream_len
        );

        if stream_len == 0 || stream_addr.is_null() {
            return Err("Invalid DSDT data");
        }

        // Set up the AmlHandler
        let aml_handler = AmlHandler;
        let mut aml_context = aml::AmlContext::new(Box::new(aml_handler), DebugVerbosity::None);

        println!("slicing...");
        // Make a slice from the mapped memory
        let stream = core::slice::from_raw_parts(stream_addr, stream_len);

        // parse the dsdt
        match aml_context.parse_table(stream) {
            Ok(_) => println!("DSDT AML Parsing successful"),
            Err(e) => {
                println!("DSDT AML parsing failed: {:?}", e);
                return Err("DSDT AML parsing failed");
            }
        };

        KernelAcpi::unmap_physical_region(&dsdt_mapping);

        Ok(aml_context)
    }
}

pub fn get_fadt() -> Result<PhysicalMapping<KernelAcpi, Fadt>, &'static str> {
    validate_phys_offset()?;
    let tables = get_acpi_tables(&KernelAcpi)?;

    match tables.find_table::<acpi::fadt::Fadt>() {
        Ok(v) => Ok(v),
        Err(e) => {
            serial_println!("Could not find FADT: {:?}", e);
            Err("Could not found FADT")
        }
    }
}
