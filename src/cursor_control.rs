use x86_64::instructions::port::Port;

use crate::vga_buffer::BUFFER_WIDTH;

/// Function for enabling the cursor with specified start and end positions
pub fn enable_cursor(cursor_start: u8, cursor_end: u8) {
    unsafe {
        Port::new(0x3D4).write(0x0A_u8);
        Port::new(0x3D5).write((Port::<u8>::new(0x3D5).read() & 0xC0) | cursor_start);

        Port::new(0x3D4).write(0x0B_u8);
        Port::new(0x3D5).write((Port::<u8>::new(0x3D5).read() & 0xE0) | cursor_end);
    }
}

/// Function for disabling the cursor
pub fn disable_cursor() {
    unsafe {
        Port::new(0x3D4).write(0x0A_u8);
        Port::new(0x3D5).write(0x20_u8);
    }
}

/// Function for updating the cursor position
pub fn update_cursor(x: usize, y: usize) {
    let pos = y * BUFFER_WIDTH + x;
    unsafe {
        Port::new(0x3D4).write(0x0F_u8);
        Port::new(0x3D5).write((pos & 0xFF) as u8);
        Port::new(0x3D4).write(0x0E_u8);
        Port::new(0x3D5).write(((pos >> 8) & 0xFF) as u8);
    }
}

/// Function for getting the cursor position
///
/// Gives you the coordinates as (x, y)
pub fn get_cursor_position() -> (u16, u16) {
    let mut pos: u16 = 0;
    unsafe {
        Port::new(0x3D4).write(0x0F_u8);
        pos |= Port::<u16>::new(0x3D5).read();
        Port::new(0x3D4).write(0x0E_u8);
        pos |= Port::<u16>::new(0x3D5).read() << 8;
    }
    (pos % BUFFER_WIDTH as u16, pos / BUFFER_WIDTH as u16)
}
