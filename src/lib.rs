// Disable standard library features and enable x86 interrupt ABI feature
#![no_std]
#![cfg_attr(test, no_main)]
#![feature(abi_x86_interrupt)]
#![feature(allocator_api)]
#![feature(custom_test_frameworks)]
#![test_runner(crate::testing::test_runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;

use core::cell::OnceCell;

#[cfg(test)]
use bootloader::{entry_point, BootInfo};

use alloc::{string::String, vec::Vec};
// use conquer_once::spin::OnceCell;
use cursor_control::enable_cursor;
use memory::BootInfoFrameAllocator;
use spin::Mutex;
use vga_buffer::Color;

use x86_64::{instructions::random::RdRand, structures::paging::OffsetPageTable};

pub mod acpi;
pub mod allocator;
pub mod applications;
pub mod clock;
pub mod cursor_control;
pub mod gdt;
pub mod interrupts;
pub mod memory;
pub mod serial;
pub mod sound;
pub mod testing;
pub mod vga_buffer;

/// Global history buffer using a spin lock
pub static HISTORY: spin::Mutex<Vec<String>> = spin::Mutex::new(Vec::new());

pub static MAPPER: Mutex<OnceCell<OffsetPageTable>> = Mutex::new(OnceCell::new());
pub static FRAME_ALLOCATOR: Mutex<OnceCell<BootInfoFrameAllocator>> = Mutex::new(OnceCell::new());

/// Initialize the system components (Global Descriptor Table, Interrupt Descriptor Table, etc.)
pub fn init() {
    gdt::init();
    interrupts::init_idt();
    unsafe { interrupts::PICS.lock().initialize() };
    x86_64::instructions::interrupts::enable();
    enable_cursor(0, 15);
    // Disable blinking text
    unsafe {
        core::arch::asm!(
            "
            mov dx, 0x3DA
            in al, dx
            mov dx, 0x3C0
            mov al, 0x30
            out dx, al
            inc dx
            in al, dx
            and al, 0xF7
            dec dx
            out dx, al
        "
        )
    }
}

/// Generate a random number in a range
pub fn random_in_range(min: u32, max: u32, default: u32) -> u32 {
    match RdRand::new() {
        Some(rng) => match rng.get_u32() {
            Some(v) => (v % (max - min + 1)) + min,
            None => default,
        },
        None => default,
    }
}

/// Main loop that repeatedly halts the CPU
pub fn hlt_loop() -> ! {
    loop {
        x86_64::instructions::hlt();
    }
}

/// Function to get user input with a prompt and optional text colors
///
/// colors is an array of type [Foreground, Background]
pub fn input(prompt: &str, colors: Option<[Color; 2]>) -> String {
    // Print the prompt
    print!("{}", prompt);

    // Set text colors if provided
    if let Some(c) = colors {
        set_color!(c[0], c[1]);
    }

    // Wait for user input
    while !*interrupts::USER_INPUT_DONE.lock() {
        x86_64::instructions::hlt();
    }

    // Reset user done variable now that we are handling the input
    let mut guard = interrupts::USER_INPUT_DONE.lock();
    *guard = false;

    // Retrieve input from the buffer
    let input_string = interrupts::INPUT_BUFFER.lock().clone();

    // Clear the input buffer
    interrupts::INPUT_BUFFER.lock().clear();

    // Print a newline for better formatting
    println!();

    // Don't add obscured inputs to history
    if !*interrupts::OBSCURE_INPUT.lock() {
        // Limit the history buffer size and store the input
        if HISTORY.lock().len() > 127 {
            HISTORY.lock().remove(0);
        }
        HISTORY.lock().push(input_string.clone());
    }

    // Return the user input
    input_string
}

#[cfg(test)]
entry_point!(test_kernel_main);

/// Entry point for `cargo test`
#[cfg(test)]
#[no_mangle]
fn test_kernel_main(_boot_info: &'static BootInfo) -> ! {
    test_main();
    loop {}
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    use testing::test_panic_handler;

    test_panic_handler(info)
}

pub fn get_single_key() -> char {
    // Suppress input display
    *interrupts::SUPPRESS_INPUT.lock() = true;

    // Wait for user to press a key
    while interrupts::INPUT_BUFFER.lock().is_empty() {
        x86_64::instructions::hlt();
    }

    // Get key from input buffer
    let key = interrupts::INPUT_BUFFER.lock().clone();

    // Clear input buffer
    interrupts::INPUT_BUFFER.lock().clear();

    // Disable input display suppression
    *interrupts::SUPPRESS_INPUT.lock() = false;

    // Return pressed key as a character
    key.chars().next().unwrap()
}
