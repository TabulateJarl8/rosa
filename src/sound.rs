use crate::clock::datetime::sleep;
use alloc::{
    string::{String, ToString},
    vec::Vec,
};
use x86_64::instructions::port::Port;

/// Musical notes for playing sound
pub enum Note {
    C,
    CSharp,
    D,
    DSharp,
    E,
    F,
    FSharp,
    G,
    GSharp,
    A,
    ASharp,
    B,
}

impl Note {
    /// Internal function to return the frequency of a note in octave 0
    fn frequency(&self) -> f32 {
        match self {
            Self::C => 16.35,
            Self::CSharp => 17.32,
            Self::D => 18.35,
            Self::DSharp => 19.45,
            Self::E => 20.60,
            Self::F => 21.83,
            Self::FSharp => 23.12,
            Self::G => 24.5,
            Self::GSharp => 25.96,
            Self::A => 27.5,
            Self::ASharp => 29.14,
            Self::B => 30.87,
        }
    }

    /// Calculate the hertz at a given octave
    pub fn hertz(&self, octave: u8) -> u32 {
        (self.frequency() * 2_u32.pow(octave.into()) as f32) as u32
    }
}

/// Play sound at a frequency out of the PC speaker
pub fn play_sound(n_frequence: u32) {
    let div: u32 = 1193180 / n_frequence;
    unsafe {
        Port::new(0x43).write(0xB6_u8);
        Port::new(0x42).write(div as u8);
        Port::new(0x42).write((div >> 8) as u8);
        let tmp: u8 = Port::new(0x61).read();

        if tmp != (tmp | 3) {
            Port::new(0x61).write(tmp | 3);
        }
    }
}

/// Stop the PC speaker from playing a sound
pub fn nosound() {
    unsafe {
        let tmp = Port::<u8>::new(0x61).read() & 0xFC;
        Port::new(0x61).write(tmp);
    }
}

/// sing a song
pub fn sing() {
    play_sound(Note::E.hertz(4));
    sleep(400000000);
    play_sound(Note::D.hertz(4));
    sleep(400000000);
    play_sound(Note::C.hertz(4));
    sleep(800000000);
    play_sound(Note::E.hertz(4));
    sleep(400000000);
    play_sound(Note::D.hertz(4));
    sleep(400000000);
    play_sound(Note::C.hertz(4));
    sleep(800000000);
    nosound();
    sleep(200000000);
    play_sound(Note::C.hertz(4));
    sleep(100000000);
    nosound();
    sleep(100000000);
    play_sound(Note::C.hertz(4));
    sleep(100000000);
    nosound();
    sleep(100000000);
    play_sound(Note::C.hertz(4));
    sleep(100000000);
    nosound();
    sleep(100000000);
    play_sound(Note::C.hertz(4));
    sleep(100000000);
    nosound();
    sleep(100000000);
    play_sound(Note::D.hertz(4));
    sleep(100000000);
    nosound();
    sleep(100000000);
    play_sound(Note::D.hertz(4));
    sleep(100000000);
    nosound();
    sleep(100000000);
    play_sound(Note::D.hertz(4));
    sleep(100000000);
    nosound();
    sleep(100000000);
    play_sound(Note::D.hertz(4));
    sleep(100000000);
    nosound();
    sleep(100000000);
    play_sound(Note::E.hertz(4));
    sleep(400000000);
    play_sound(Note::D.hertz(4));
    sleep(400000000);
    play_sound(Note::C.hertz(4));
    sleep(800000000);
    nosound();
}

/// Play a F# Major scale
pub fn fsharp_scale() {
    play_sound(Note::FSharp.hertz(4));
    sleep(100000000);
    nosound();
    sleep(100000000);

    play_sound(Note::GSharp.hertz(4));
    sleep(100000000);
    nosound();
    sleep(100000000);

    play_sound(Note::ASharp.hertz(4));
    sleep(100000000);
    nosound();
    sleep(100000000);

    play_sound(Note::B.hertz(4));
    sleep(100000000);
    nosound();
    sleep(100000000);

    play_sound(Note::CSharp.hertz(5));
    sleep(100000000);
    nosound();
    sleep(100000000);

    play_sound(Note::DSharp.hertz(5));
    sleep(100000000);
    nosound();
    sleep(100000000);

    play_sound(Note::F.hertz(5));
    sleep(100000000);
    nosound();
    sleep(100000000);

    play_sound(Note::FSharp.hertz(5));
    sleep(100000000);
    nosound();
    sleep(100000000);
}

pub fn play(song_raw: &str) {
    // Split title, parameters, and song
    let song_parts: Vec<&str> = song_raw.split(':').collect();

    // Get parameters
    let parameters: Vec<&str> = song_parts[1].trim().split(',').collect::<Vec<&str>>();

    // Parse parameters
    let default_duration = parameters[0].split('=').collect::<Vec<&str>>()[1]
        .parse::<u8>()
        .unwrap_or(4);
    let default_octave = parameters[1].split('=').collect::<Vec<&str>>()[1]
        .parse::<u8>()
        .unwrap_or(5);
    let bpm = parameters[2].split('=').collect::<Vec<&str>>()[1]
        .parse::<u8>()
        .unwrap_or(100);

    // Get notes
    let song = song_parts[2];
    let notes: Vec<&str> = song.trim().split(',').collect();

    for note in notes {
        // Remove whitepace
        let note = note.trim();
        // println!("{}", note);

        // Store how many characters we've read so we know where to read future ones
        let mut offset = 0;

        // Get duration
        let mut duration: u8 = if note.starts_with("16") {
            16
        } else if note.starts_with("32") {
            32
        } else {
            note.chars()
                .next()
                .unwrap()
                .to_string()
                .parse::<u8>()
                .unwrap_or(0)
        };

        if duration > 9 {
            offset += 2;
        } else if duration < 1 {
            offset = 0;
            duration = default_duration;
        } else {
            offset += 1;
        }

        // Get letter
        let mut letter: String = note.chars().nth(offset).unwrap().to_string();
        offset += 1;

        let sharp = note.chars().nth(offset).unwrap_or_default() == '#';
        if sharp {
            offset += 1;
            letter += "#";
        }

        // Dot might come before octave, increase the offset if it does
        if note.chars().nth(offset).unwrap_or_default() == '.' {
            offset += 1
        }

        // Get octave
        let octave = note
            .chars()
            .nth(offset)
            .unwrap_or('x')
            .to_string()
            .parse::<u8>()
            .unwrap_or(default_octave);

        // Check if dotted
        let dotted = note.contains('.');

        let mut rest = false;

        // Make note into real note
        let pitch = match letter.as_str() {
            "a" => crate::sound::Note::A,
            "a#" => crate::sound::Note::ASharp,
            "b" => crate::sound::Note::B,
            "c" => crate::sound::Note::C,
            "c#" => crate::sound::Note::CSharp,
            "d" => crate::sound::Note::D,
            "d#" => crate::sound::Note::DSharp,
            "e" => crate::sound::Note::E,
            "f" => crate::sound::Note::F,
            "f#" => crate::sound::Note::FSharp,
            "g" => crate::sound::Note::G,
            "g#" => crate::sound::Note::GSharp,
            "p" => {
                rest = true;
                crate::sound::Note::ASharp
            }
            _ => crate::sound::Note::ASharp,
        };

        // Play sound unless this note is a rest
        if !rest {
            play_sound(pitch.hertz(octave));
        } else {
            nosound();
        }
        sleep(
            ((150000000000.0 / duration as f64 / bpm as f64) * (1.0 + 0.5 * dotted as u8 as f64))
                as u64,
        );
        nosound();
        sleep((1000000000.0 / bpm as f64) as u64);
    }
    nosound()
}
