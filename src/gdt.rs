use core::ptr::addr_of;

// global descriptor table
use lazy_static::lazy_static;
use x86_64::{
    structures::{
        gdt::{Descriptor, GlobalDescriptorTable, SegmentSelector},
        tss::TaskStateSegment,
    },
    VirtAddr,
};

/// Index for the interrupt stack table in the Task State Segment (TSS) for double faults.
pub const DOUBLE_FAULT_IST_INDEX: u16 = 0;

/// Struct holding the selectors for code and the task state segment (TSS).
struct Selectors {
    /// Selector for the kernel code segment.
    code_selector: SegmentSelector,
    /// Selector for the task state segment (TSS).
    tss_selector: SegmentSelector,
}

lazy_static! {
    /// Task State Segment (TSS) for managing interrupts and exceptions.
    static ref TSS: TaskStateSegment = {
        let mut tss = TaskStateSegment::new();
        // Set up the stack for double faults
        tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX as usize] = {
            const STACK_SIZE: usize = 4096 * 5;
            static mut STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];

            let stack_start = VirtAddr::from_ptr(addr_of!(STACK));
            // stack end
            stack_start + STACK_SIZE
        };
        tss
    };
}

lazy_static! {
    /// Global Descriptor Table (GDT) and corresponding selectors.
    static ref GDT: (GlobalDescriptorTable, Selectors) = {
        let mut gdt = GlobalDescriptorTable::new();
        // Add kernel code segment entry to GDT
        let code_selector = gdt.add_entry(Descriptor::kernel_code_segment());
        // Add TSS entry to GDT
        let tss_selector = gdt.add_entry(Descriptor::tss_segment(&TSS));
        (
            gdt,
            Selectors {
                code_selector,
                tss_selector,
            },
        )
    };
}

/// Initializes the Global Descriptor Table (GDT) and sets up the task state segment (TSS).
///
/// This function loads the GDT and sets the code selector and TSS selector for use in the system.
///
/// # Safety
///
/// This function uses unsafe operations for low-level system initialization.
pub fn init() {
    use x86_64::instructions::{
        segmentation::{Segment, CS},
        tables::load_tss,
    };

    GDT.0.load();
    // Set the code selector for CS register and load TSS
    unsafe {
        CS::set_reg(GDT.1.code_selector);
        load_tss(GDT.1.tss_selector);
    }
}
