#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(rosa::testing::test_runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;

use alloc::vec::Vec;
use bootloader::{entry_point, BootInfo};
use core::panic::PanicInfo;
use hashbrown::HashMap;
use rosa::{
    acpi::tables::init_acpi,
    allocator,
    applications::*,
    clock,
    memory::{self, BootInfoFrameAllocator},
    print, println, set_color,
    vga_buffer::Color,
    FRAME_ALLOCATOR, MAPPER,
};
use x86_64::VirtAddr;

// panic handler
#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    // Print fancy error screen
    set_color!(Color::White, Color::LightRed);
    rosa::clear_screen!();
    println!("Rosa has encountered an error and cannot continue\n\n");
    println!("{}", info);
    rosa::hlt_loop();
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    rosa::testing::test_panic_handler(info)
}

fn merge_maps<K, V>(target: &mut HashMap<K, V>, source: HashMap<K, V>)
where
    K: core::hash::Hash,
    K: core::cmp::Eq,
{
    for (key, value) in source {
        target.insert(key, value);
    }
}

const MESSAGES: [&str; 13] = [
    "Rust-ing Your Computer's Boredom Away!",
    "Rusty, Trusty, and Simple - Just the Way You Like It!",
    "Rust the Process",
    "Glutenful",
    "In god we Rust",
    "Contains 100% more rosafetch than Linux!",
    "Don't look under the heap!",
    "Contains a more maintainable calculator than iiCalc!",
    "The only OS to provide bundled BaboonSeeker!",
    "Bulletproof!",
    "Discoproof!",
    "Rosa train 'toot-toot' laying brass",
    "Keep arms and legs inside the operating system at all times",
];

entry_point!(kernel_main);

fn kernel_main(boot_info: &'static BootInfo) -> ! {
    #[cfg(test)]
    test_main();

    print!("Welcome to Rosa: ");
    set_color!(Color::LightRed, Color::Black);
    println!(
        "{}",
        MESSAGES[rosa::random_in_range(0, MESSAGES.len() as u32 - 1, 0) as usize]
    );
    set_color!(Color::LightBlue, Color::Black);
    println!("https://gitlab.com/TabulateJarl8/Rosa");
    set_color!(Color::White, Color::Black);
    println!("Press F12 at any time to dump input buffer");

    rosa::init();

    // create a framebuffer allocator and a memory mapper
    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);

    // address mapper
    let mapper = unsafe { memory::init(phys_mem_offset) };
    MAPPER.lock().get_or_init(|| mapper);

    // frame allocator
    let frame_allocator = unsafe { BootInfoFrameAllocator::new(&boot_info.memory_map) };
    FRAME_ALLOCATOR.lock().get_or_init(|| frame_allocator);

    // initialize the heap
    allocator::init_heap(
        MAPPER.lock().get_mut().unwrap(),
        FRAME_ALLOCATOR.lock().get_mut().unwrap(),
    )
    .expect("heap initialization failed");

    // initialize the ACPI
    let cloned_offset = boot_info.physical_memory_offset;
    init_acpi(cloned_offset).unwrap();

    // Start counting uptime
    let _ = clock::uptime::get_uptime();

    // Set up hashmap for commands
    let mut commands = HashMap::new();
    // call merge_maps and pass it each module's get_commands
    automod::add_commands_to_hashmap!("src/applications");

    // Startup sound
    //rosa::sound::play("s:d=4,o=5,b=175:16c#,16d,16f,4a#");

    loop {
        // Command input
        set_color!(Color::Red, Color::Black);
        let input_text = rosa::input("Rosa Shell> ", Some([Color::White, Color::Black]));

        // Split command into parts
        let command: Vec<&str> = input_text.split(' ').collect();

        // Handle command
        if commands.contains_key(command[0]) {
            // Command found, run it
            commands.get(command[0]).unwrap().0(&command[1..]);
        } else if command[0].is_empty() {
            // Ignore empty command
        } else if command[0] == "help" {
            // List available commands
            println!("Available commands: ");

            // get longest command length and add 2 for padding
            let longest_command_length = match commands.keys().max_by_key(|&&x| x.len()) {
                Some(v) => v.len(),
                None => 0,
            } + 2;

            // Sort commands alphabetically
            let mut sorted_commands = commands
                .iter()
                .collect::<Vec<(&&str, &(fn(&[&str]), &str))>>();
            sorted_commands.sort();

            // Print commands and short descriptions
            for command in &sorted_commands {
                set_color!(Color::White, Color::Black);
                print!("{: <1$}", command.0, longest_command_length);
                set_color!(Color::LightGrey, Color::Black);
                println!("{}", command.1 .1);
            }
        } else {
            // Command not found
            println!("Command {} not found", command[0]);
        }
    }
}
