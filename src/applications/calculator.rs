use alloc::vec::Vec;

use crate::{clear_screen, command_alias, input, println, set_color, vga_buffer::Color};

extern crate alloc;

use super::CommandMap;

// Calculator
pub fn calculator(_: &[&str]) {
    // Initialize stack
    let mut stack: Vec<f64> = Default::default();

    // Set colors
    set_color!(Color::Black, Color::White);
    // Clear screen with new colors
    clear_screen!();

    // Welcome message
    println!("Rosa Calculator");
    println!("Uses RPN input");
    println!("Available operations: +, -, *, /, d/drop, [enter]/dup, exit/quit \n\n");

    // The calculator itself
    'calculator: loop {
        // Set color for input prompt
        set_color!(Color::Black, Color::White);

        // Input
        let command = input("Rosa Calculator> ", Some([Color::Brown, Color::White]));

        // Try to parse as float first
        match command.parse::<f64>() {
            // Successfully parsed as float, add to stack
            Ok(float) => {
                stack.push(float);
            }
            // Not a float, parse as a command
            Err(_) => {
                match command.as_str() {
                    // Add
                    "+" => {
                        // Make sure there are items on stack
                        if stack.len() >= 2 {
                            // Get operands
                            let b = stack.pop().unwrap();
                            let a = stack.pop().unwrap();

                            // Perform operation and put on stack
                            stack.push(a + b);
                        }
                    }
                    // Subtract
                    "-" => {
                        // Make sure there are items on stack
                        if stack.len() >= 2 {
                            // Get operands
                            let b = stack.pop().unwrap();
                            let a = stack.pop().unwrap();

                            // Perform operation and put on stack
                            stack.push(a - b);
                        }
                    }
                    // Multiply
                    "*" => {
                        // Make sure there are items on stack
                        if stack.len() >= 2 {
                            // Get operands
                            let b = stack.pop().unwrap();
                            let a = stack.pop().unwrap();

                            // Perform operation and put on stack
                            stack.push(a * b);
                        }
                    }
                    // Divide
                    "/" => {
                        // Make sure there are items on stack
                        if stack.len() >= 2 {
                            // Get operands
                            let b = stack.pop().unwrap();
                            let a = stack.pop().unwrap();

                            // Perform operation and put on stack
                            stack.push(a / b);
                        }
                    }
                    //Drop
                    "d" | "drop" => {
                        // Make sure there is an item on the stack
                        if !stack.is_empty() {
                            // Drop most recent item
                            stack.pop();
                        }
                    }
                    // Dup
                    "" | "dup" => {
                        // Make sure there is an item on the stack
                        if !stack.is_empty() {
                            // Get most recent item
                            let a = stack.pop().unwrap();
                            // Push to stack twice
                            stack.push(a);
                            stack.push(a);
                        }
                    }
                    // Exit calculator
                    "exit" | "quit" => break 'calculator,
                    // Ignore invalid commands
                    _ => (),
                }
            }
        }

        // Print stack
        for item in &stack {
            println!("{}", item);
        }
    }

    // Clear screen again after exit
    set_color!(Color::White, Color::Black);
    clear_screen!();
}

// Set up commands
pub fn get_commands() -> CommandMap {
    command_alias! {
        "calc" => (calculator, "Calculator"),
        "calculator" => (calculator, "Calculator")
    }
}
