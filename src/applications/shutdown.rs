use crate::{acpi::power::acpi_shutdown, command_alias};

use super::CommandMap;

// Print date
pub fn shutdown(_args: &[&str]) {
    acpi_shutdown();
}

// Set up commands
pub fn get_commands() -> CommandMap {
    command_alias! {
        "shutdown" => (shutdown, "Shutdown the computer")
    }
}
