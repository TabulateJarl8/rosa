use alloc::{vec::Vec, string::String};

use crate::{clear_screen, command_alias, input, println, print, set_color, vga_buffer::Color};

extern crate alloc;

use super::CommandMap;

struct Interpreter{
    // Stores whether the interpreter is still in use or has been exited
    alive: bool,
    // Stores whether the interpreter is currently executing (false) or defining a word (true)
    defining_word: bool,
    // Stores the name of the word currently being defined
    word_name: String,
    // Stores words
    words: hashbrown::HashMap<String, String>,
    // Stores whether the interpreter is currently outputting a string
    string_output_mode: bool,
    // Stores the level of the current conditional, zero if we are not currently in a conditional
    conditional_level: u16,
    // Stores whether the current conditional is active (condition was true or we have gotten to else)
    conditional_active: Vec<bool>,
    // Stores the level of the current loop, zero if we are not currently in a loop
    loop_level: u16,
    // Stores the counters for loops
    loop_counters: Vec<i32>,
    // Stores the ending values for loops
    loop_ends: Vec<i32>,
    // Loop contents
    loop_contents: Vec<String>,
    // Variable names
    variable_names: hashbrown::HashMap<String, usize>,
    // Variable values
    variable_values: [f64; 512],
    // Number of used variables
    used_variables: usize,
    // Whether we are currently defining a variable
    defining_variable: bool,
    // Constants
    constants: hashbrown::HashMap<String, f64>,
    // Whether we are currently defining a constant
    defining_constant: bool,
    // Stores the interpreter's stack'
    stack: Vec<f64>,
}

impl Interpreter{

    // Intialize a new interpreter object and return it
    pub fn new() -> Self{
        Interpreter{
            alive: true,
            defining_word: false,
            word_name: String::from(""),
            words: hashbrown::HashMap::new(),
            string_output_mode: false,
            conditional_level: 0,
            conditional_active: Vec::new(),
            loop_level: 0,
            loop_counters: Vec::new(),
            loop_ends: Vec::new(),
            loop_contents: Vec::new(),
            variable_names: hashbrown::HashMap::new(),
            variable_values: [0 as f64; 512],
            used_variables: 0,
            defining_variable: false,
            constants: hashbrown::HashMap::new(),
            defining_constant: false,
            stack: Vec::new()
        }
    }

    fn get_operands(&mut self, number: usize) -> Vec<f64>{
        let mut operands = Vec::<f64>::new();

        for _ in 0..number{
            operands.push(self.stack.pop().unwrap());
        }

        // Change the order of the operands to be the reverse of the order in which they were taken from the stack
        // This will be the order they were put into the stack
        operands.reverse();

        // Return operands
        operands
    }

    fn add_to_definition(&mut self, token: &str){
        // Check if we have a name for our word yet
        if self.word_name.is_empty(){
            // We don't, this token must be its name, create a new word with this name
            self.word_name = String::from(token);
            self.words.insert(self.word_name.clone(), String::from(""));
        } else if token == ";" {
            // We do, and this is the end of the word definition

            // We are no longer defining, reset back to executing state
            self.defining_word = false;
            self.word_name = String::from("");

        } else {
            // We do, and this is an operation to go into the word definition

            // Get current word definition
            let current_definition = self.words.get(&self.word_name).unwrap();

            let space = if current_definition.is_empty() {""} else {" "};

            // Append new token to definition
            self.words.insert(self.word_name.clone(), current_definition.clone() + space + token);
        }
    }

    fn add(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        // Perform operation and add result to the stack
        self.stack.push(operands[0] + operands[1]);
    }

    fn subtract(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        // Perform operation and add result to the stack
        self.stack.push(operands[0] - operands[1]);
    }

    fn multiply(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        // Perform operation and add result to the stack
        self.stack.push(operands[0] * operands[1]);
    }

    fn divide(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        // Perform operation and add result to the stack
        self.stack.push(operands[0] / operands[1]);
    }

    fn modulo(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        // Perform operation and add result to the stack
        self.stack.push(operands[0] % operands[1]);
    }

    fn equals(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        // Check equality, giving -1 if equal and 0 if not equal
        let result = if operands[0] == operands[1] { -1 } else { 0 };

        // Put result on stack
        self.stack.push(result as f64);
    }

    fn gt(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        // Check equality, giving -1 if equal and 0 if not equal
        let result = if operands[0] > operands[1] { -1 } else { 0 };

        // Put result on stack
        self.stack.push(result as f64);
    }

    fn lt(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        // Check equality, giving -1 if equal and 0 if not equal
        let result = if operands[0] < operands[1] { -1 } else { 0 };

        // Put result on stack
        self.stack.push(result as f64);
    }

    fn and(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        let result = (operands[0] as i32) & (operands[1] as i32);

        // Put result on stack
        self.stack.push(result as f64);
    }

    fn or(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        let result = (operands[0] as i32) | (operands[1] as i32);

        // Put result on stack
        self.stack.push(result as f64);
    }

    fn lshift(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        let result = (operands[0] as i32) << (operands[1] as i32);

        // Put result on stack
        self.stack.push(result as f64);
    }

    fn rshift(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        let result = (operands[0] as i32) >> (operands[1] as i32);

        // Put result on stack
        self.stack.push(result as f64);
    }

    fn invert(&mut self){
        // Get operand from stack
        let operand = self.get_operands(1)[0];

        // Invert bits
        let result = ! (operand as i64);

        // Put result on stack
        self.stack.push(result as f64);
    }

    fn nip(&mut self) {
        self.stack.remove(self.stack.len() - 2);
    }

    fn begin_if(&mut self){
        // Get operand from stack
        let operand = self.get_operands(1)[0];

        // Increase conditional level
        self.conditional_level += 1;

        // Check condition and set active indicator for this concidional level (0=false, others = true)
        if operand == 0 as f64 {
            self.conditional_active.push(false);
        } else {
            self.conditional_active.push(true);
        }
    }

    fn begin_do(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        // Increase loop level
        self.loop_level += 1;

        // Initialize loop counter
        self.loop_counters.push(operands[1] as i32);

        // Store end value for counter
        self.loop_ends.push(operands[0] as i32);

        // Initialize string for this loop
        self.loop_contents.push(String::new());
    }

    fn begin_begin(&mut self){
        // Increase loop level
        self.loop_level += 1;

        // Use special starting and ending values of 1 and -1 to indicate this is a begin loop not a do loop
        self.loop_counters.push(1_i32);
        self.loop_ends.push(-1_i32);

        // Initialize string for this loop
        self.loop_contents.push(String::new());
    }

    fn dup(&mut self){
        // Get operand from stack
        let operand = self.get_operands(1)[0];

        // Place back on stack twice
        self.stack.push(operand);
        self.stack.push(operand);
    }

    fn drop(&mut self){
        // Take operand from stack and don't do anything with it
        let _ = self.get_operands(1)[0];
    }

    fn swap(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        // Place on stack in reverse order
        self.stack.push(operands[1]);
        self.stack.push(operands[0]);
    }

    fn over(&mut self){
        // Get second last item from stack (Without removing it)
        let item = self.stack[self.stack.len() - 2];

        // Add to stack
        self.stack.push(item);
    }

    fn rot(&mut self){
        // Get operands
        let operands = self.get_operands(3);

        // Push back to stack, but with the first item last
        self.stack.push(operands[1]);
        self.stack.push(operands[2]);
        self.stack.push(operands[0]);
    }

    fn print(&mut self){
        // Get operand from stack
        let operand = self.get_operands(1)[0];

        // Print to screen
        print!("{}", operand);
    }

    fn emit(&mut self){
        // Get operand from stack
        let operand = self.get_operands(1)[0];

        // Print to screen as byte
        crate::vga_buffer::print_byte(operand as u8);
    }

    fn get_counter(&mut self){
        // Get counter from counters vector
        let counter = self.loop_counters[(self.loop_level - 1) as usize];

        // Put counter on stack
        self.stack.push(counter as f64);

    }

    // List currently defined words
    fn list_words(&mut self){
        println!("Words:");
        for word in self.words.keys(){
            println!("{}: {}", word, self.words[word]);
        }
    }

    fn set_var(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        // Write to variable
        self.variable_values[operands[1] as usize] = operands[0];
    }

    fn add_to_var(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        // Increment variable
        self.variable_values[operands[1] as usize] += operands[0];
    }

    fn get_var(&mut self){
        // Get variable location from stack
        let operand = self.get_operands(1)[0];

        // Get variable value from location and put it on the stack
        self.stack.push(self.variable_values[operand as usize]);
    }

    fn allot(&mut self){
        // Get operand from stack
        let operand = self.get_operands(1)[0];

        // Allocate this many cells
        self.used_variables += operand as usize;
    }

    fn random(&mut self){
        // Get operands from stack
        let operands = self.get_operands(2);

        // Generate random number
        self.stack.push(crate::random_in_range(operands[1] as u32, operands[0] as u32, 0) as f64);
    }

    // Evaluate a token
    pub fn eval(&mut self, raw_token: &str, my_loop_level: u16) ->Result<(), ()>{

        let token = raw_token.to_lowercase();

        // Stop execution if user has requested we break
        if *crate::interrupts::BREAK_REQUESTED.lock(){
            *crate::interrupts::BREAK_REQUESTED.lock() = false;
            return Err(());
        }

        // Discard the current word, conditional, and loop, returning to normal execution mode
        if token == "discard"{
            self.defining_word = false;
            self.word_name = String::new();
            self.conditional_level = 0;
            self.conditional_active.clear();
            self.loop_level = 0;
            self.loop_counters.clear();
            self.loop_ends.clear();
            self.loop_contents.clear();
            self.defining_variable = false;
            self.defining_constant = false;
        }

        // Check whether we are currently defining a word
        if self.defining_word {
            // Defining
            self.add_to_definition(token.as_str());
            Ok(())

        } else {
            // Executing

            // Check if we are currently in a conditional
            if self.conditional_level > 0{
                // If this is the end of the conditional, decrease the conditional level, remove the activity status for this level, and finish evaluation
                if token == "then" {
                    self.conditional_active.remove((self.conditional_level - 1) as usize);
                    self.conditional_level -= 1;
                    return Ok(());
                }

                // If this is an else, invert the current conditional level active status and finish evaluation
                if token == "else"{
                    self.conditional_active[(self.conditional_level - 1) as usize] = ! self.conditional_active[(self.conditional_level - 1) as usize];
                    return Ok(());
                }

                // If the current conditional level is not currently active, we should not execute this token. End evaluation
                if ! self.conditional_active[(self.conditional_level - 1) as usize]{
                    return Ok(())
                }
            }

            // Check if we are currently in a loop at a higher level than ourselves
            if self.loop_level > my_loop_level {
                // Check for our magic weird numbers to see if this is an until loop
                let is_until = self.loop_counters[(self.loop_level - 1) as usize] == 1 && self.loop_ends[(self.loop_level - 1) as usize] == -1;

                // Check number of DOs in our if
                let mut dos = self.loop_contents[(self.loop_level - 1) as usize].matches("do ").count();
                dos += self.loop_contents[(self.loop_level - 1) as usize].matches("begin ").count();

                // Check number of LOOPs in our if
                let mut loops = self.loop_contents[(self.loop_level - 1) as usize].matches(" loop").count();
                loops += self.loop_contents[(self.loop_level - 1) as usize].matches(" until").count();

                // If this is the end of our loop, execute it and then clean up
                if dos == loops && (token == "loop" && ! is_until || token == "until" && is_until) {
                    match token.as_str() {
                        // This is a do/loop loop
                        "loop" => {
                            let this_loop = self.loop_contents[(self.loop_level - 1) as usize].clone();

                            // Get starting and ending values for this loop
                            // I'm not sure why we have to add 1 to the starting value here
                            let starting_value = self.loop_counters[(self.loop_level - 1) as usize];
                            let ending_value = self.loop_ends[(self.loop_level - 1) as usize];

                            // Execute loop
                            for i in starting_value..ending_value{
                                // Set loop counter so it can be accessed from within the loop
                                self.loop_counters[(self.loop_level - 1) as usize] = i;

                                // Execute all tokens in the loop
                                for loop_token in this_loop.split(' '){
                                    match self.eval(loop_token, my_loop_level + 1) {
                                        Ok(()) => (),
                                        Err(()) => return Err(())
                                    }
                                }
                            }
                        }
                        // This is begin/until loop
                        "until" => {
                            let this_loop = self.loop_contents[(self.loop_level - 1) as usize].clone();

                            // Execute all tokens in the loop once
                            for loop_token in this_loop.split(' '){
                                match self.eval(loop_token, my_loop_level + 1) {
                                    Ok(()) => (),
                                    Err(()) => return Err(())
                                }
                            }

                            // Repeatedly execute until we get a number other than zero as the last on the stack
                            while self.stack.pop() == Some(0 as f64) {
                                // Execute all tokens in the loop once
                                for loop_token in this_loop.split(' '){
                                    match self.eval(loop_token, my_loop_level + 1) {
                                        Ok(()) => (),
                                        Err(()) => return Err(())
                                    }
                                }
                            }
                        }
                        _ => ()
                    }

                    // Clean up loop information now that this loop is done
                    self.loop_counters.remove((self.loop_level - 1) as usize);
                    self.loop_ends.remove((self.loop_level - 1) as usize);
                    self.loop_contents.remove((self.loop_level - 1) as usize);

                    // Decrease loop level
                    self.loop_level -= 1;

                    // Finish evaluation
                    return Ok(())
                }

                // If this is not the end of the loop, add this token to the loop

                // Add separating space if the loop is not empty
                if !self.loop_contents[(self.loop_level - 1) as usize].is_empty() {
                    self.loop_contents[(self.loop_level - 1) as usize] += " ";
                }

                // Add to loop contents
                self.loop_contents[(self.loop_level - 1) as usize] += token.as_str();

                // Finish evaluation since we aren't actually executing here, just preparing to loop
                return Ok(())
            }

            // Check if we are currently outputting a string instead of executing
            if self.string_output_mode{
                 let mut token_chars = token.chars();
                // Check if this is the last token of the string
                if token_chars.clone().last().unwrap() == '"'{
                    // It is, we are no longer in output mode
                    self.string_output_mode = false;

                    // Remove the quote from the end
                    token_chars.next_back();

                }

                // Print token
                print!("{} ", token_chars.as_str());

                // Done evaulating, return without actually trying to match token since it is a string
                return Ok(())
            }

            // Check if we are currently defining a variable. If we are, the token we just recieved is its name
            if self.defining_variable{
                // Assign a location to the variable
                self.variable_names.insert(token, self.used_variables);

                // Increment used variables
                self.used_variables += 1;

                // Done defining variable
                self.defining_variable = false;

                // End evaluation, since this token was a variable name, not something to execute
                return Ok(())
            }

            // Check if we are currently defining a constant. If we are, the token we just recieved is its name
            if self.defining_constant{
                // Get value from stack
                let value = self.get_operands(1)[0];

                // Put into constants
                self.constants.insert(token, value);

                // Done defining variable
                self.defining_constant = false;

                // End evaluation, since this token was a constant name, not something to execute
                return Ok(())
            }

            // Match token without case sensitivity
            match token.clone().as_str(){
                // Add
                // Requires at least two items on the stack
                "+" if self.stack.len() >= 2 => {
                self.add();
                }
                // Subtract
                // Requires at least two items on the stack
                "-" if self.stack.len() >= 2 => {
                    self.subtract();
                }
                // Multiply
                // Requires at least two items on the stack
                "*" if self.stack.len() >= 2 => {
                    self.multiply();
                }
                // Divide
                // Requires at least two items on the stack
                "/" if self.stack.len() >= 2 => {
                    self.divide();
                }
                // Modulo
                // Requires at least two items on the stack
                "mod" if self.stack.len() >= 2 => {
                    self.modulo();
                }
                // Check if the last two stack items are equal
                // Requires at least two items on the stack
                "=" if self.stack.len() >= 2 => {
                    self.equals();
                }
                // Check if the second last item on the stack is greater than the last item
                // Requires at least two items on the stack
                ">" if self.stack.len() >= 2 => {
                    self.gt();
                }
                // Check if the second last item on the stack is less than the last item
                // Requires at least two items on the stack
                "<" if self.stack.len() >= 2 => {
                    self.lt();
                }
                // Perform bitwise and on last two items
                // Requires at least two items on the stack
                "and" if self.stack.len() >= 2 => {
                    self.and();
                }
                // Perform bitwise or on last two items
                // Requires at least two items on the stack
                "or" if self.stack.len() >= 2 => {
                    self.or();
                }
                // Perform bitwise lshift on last two items
                // Requires at least two items on the stack
                "lshift" if self.stack.len() >= 2 => {
                    self.lshift();
                }
                // Perform bitwise rshift on last two items
                // Requires at least two items on the stack
                "rshift" if self.stack.len() >= 2 => {
                    self.rshift();
                }
                // Performs bitwise invert on last item
                // Requires at least one item on the stack
                "invert" if ! self.stack.is_empty() => {
                    self.invert();
                }
                // Beginning of a conditional
                // Requires at least one item on the stack
                "if" if ! self.stack.is_empty() => {
                    self.begin_if();
                }
                // Beginning of a do loop
                // Requires at least two items on the stack
                "do" if self.stack.len() >= 2 => {
                    self.begin_do();

                }
                // Beginning of a begin loop
                "begin" => {
                    self.begin_begin();
                }
                // Get loop counter
                // Requires we are in a loop
                "i" if self.loop_level > 0 => {
                    self.get_counter();
                }
                // Duplicate last item on stack
                // Requires at least one item on the stack
                "dup" if ! self.stack.is_empty() => {
                    self.dup();
                }
                // Drop last item from stack
                // Requires at least one item on the stack
                "drop" if ! self.stack.is_empty() => {
                    self.drop();
                }
                // Drop the second to last item on the stack
                // Requires at least 2 items on the stack
                "nip" if self.stack.len() >= 2 => {
                    self.nip();
                }
                // Swap the last two items on the stack
                // Requires at least two items on the stack
                "swap" if self.stack.len() >= 2 => {
                    self.swap();
                }
                // Get the second last item from the stack and duplicate it to the end of the stack
                // Requires at least two items on the stack
                "over" if self.stack.len() >= 2 => {
                    self.over();
                }
                // Rotate last three items in stack
                // Requires at least three items in the stack
                "rot" if self.stack.len() >= 3 => {
                    self.rot();
                }
                // Print the last item in the stack to the screen
                // Requires at least one item in the stack
                "." if ! self.stack.is_empty() => {
                    self.print();
                }
                // Print last item in stack as the corresponding ascii character
                // Requires at least one item in the stack
                "emit" if ! self.stack.is_empty() => {
                    self.emit();
                }
                // Print carriage return
                "cr" => {
                    println!();
                }
                // Begin word definition
                ":" => {
                    self.defining_word = true;
                }
                // Begin string output
                ".\"" => {
                    self.string_output_mode = true;
                }
                // Begin constant definition
                // Requires at least one item on the stack
                "constant" if ! self.stack.is_empty() => {
                    self.defining_constant = true;
                }
                // Begin variable definition
                "variable" => {
                    self.defining_variable = true;
                }
                // Allocate an array after a variable
                // Requires at least one item on the stack
                "allot" if ! self.stack.is_empty() => {
                    self.allot();
                }
                // Take keyboard input
                "key" => {
                    // Wait for keypress and capture it
                    let key = crate::get_single_key();

                    // Buffer to store unicode representation of key
                    let mut keycode: [u8;1] = [0; 1];

                    // Encode key
                    let _ = key.encode_utf8(&mut keycode);

                    // Put keypress keycode on stack
                    self.stack.push(keycode[0] as f64);
                }
                // Set variable value
                // Requires at least two items on the stack
                "!" if self.stack.len() >= 2 && (0..self.variable_values.len()).contains(&(*self.stack.last().unwrap() as usize))  => {
                    self.set_var();
                }
                // Add to variable
                // Requires at least two items on the stack
                "+!" if self.stack.len() >= 2 && (0..self.variable_values.len()).contains(&(*self.stack.last().unwrap() as usize)) => {
                    self.add_to_var();
                }
                // Get and display variable
                // Requires at least one item on the stack
                "?" if ! self.stack.is_empty() && (0..self.variable_values.len()).contains(&(*self.stack.last().unwrap() as usize)) => {
                    self.get_var();
                    self.print();
                }
                // Get variable value
                // Requires at least one item on the stack
                "@" if ! self.stack.is_empty() && (0..self.variable_values.len()).contains(&(*self.stack.last().unwrap() as usize)) => {
                    self.get_var();
                }
                // Generate random number in range
                // Requires at least two items on the stack
                "random" if self.stack.len() >= 2 => {
                    self.random();
                }
                // Show words
                "words" => {
                    self.list_words();
                }
                // Exit the interpreter
                "bye" => self.alive = false,
                // User defined word, execute
                _ if self.words.contains_key(token.as_str()) => {
                    // Get word contents
                    let word = self.words.get(token.as_str()).unwrap();

                    // Iterate through tokens in word and execute them
                    for word_token in word.clone().split(' ') {
                        // Execute, stopping and returning Err if the child did
                        match self.eval(word_token, my_loop_level) {
                            Ok(()) => (),
                            Err(()) => return Err(())
                        }
                    }
                }
                // User defined constant, get value
                _ if self.constants.contains_key(token.as_str()) => {
                    self.stack.push(*self.constants.get(token.as_str()).unwrap());
                }
                // User defined variable, get location
                _ if self.variable_names.contains_key(token.as_str()) => {
                    self.stack.push(*self.variable_names.get(token.as_str()).unwrap() as f64);
                }
                // Either invalid or a number
                _ => {
                    if let Ok(number) = token.parse::<f64>() { self.stack.push(number) }
                }
            }
            Ok(())
        }
    }
}

pub fn main(_: &[&str]){
    // Initialize interpreter
    let mut interpreter = Interpreter::new();

    // Clear screen and welcome user
    clear_screen!();
    set_color!(Color::Green, Color::Black);
    println!("Welcome to Rosa Forth");
    println!("Exit using 'bye'");

    // Keep running until interpreter exits
    while interpreter.alive{

        // Set color for stack and prompt
        set_color!(Color::White, Color::Black);

        // Print empty lines so stack isn't confused with program output'
        print!("\n\n");
        //Print stack
        for item in &interpreter.stack{
            println!("{}", item);
        }

        // Warn user and set special color if currently in a state other than executing
        if interpreter.loop_level > 0 {
            set_color!(Color::Cyan, Color::Black);
            println!("Defining loop: {}", interpreter.loop_contents[(interpreter.loop_level - 1) as usize]);
        }

        if interpreter.defining_word {
            set_color!(Color::Yellow, Color::Black);
            println!("Defining word: {}", interpreter.word_name);
        }

        if interpreter.defining_constant {
            set_color!(Color::LightCyan, Color::Black);
            println!("Constant name:");
        }

        if interpreter.defining_variable {
            set_color!(Color::Pink, Color::Black);
            println!("Variable name:");
        }

        if interpreter.string_output_mode {
            set_color!(Color::LightBlue, Color::Black);
            println!("String output mode");
        }


        // Accept command from user
        let command = input("Rosa Forth> ", Some([Color::Green, Color::Black]));

        // Set color for program output
        set_color!(Color::LightGreen, Color::Black);

        // Split command into tokens separated by space
        for token in command.split(' '){
            // Evaluate each token
            match interpreter.eval(token, 0) {
                Ok(()) => (),
                Err(()) => println!("\nBreak")
            }
        }
    }
}

// Set up commands
pub fn get_commands() -> CommandMap {
    command_alias! {
        "forth" => (main, "Rosa Forth interpreter"),
        "fth" => (main, "Rosa Forth interpreter")
    }
}
