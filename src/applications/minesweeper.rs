use core::fmt::Debug;

use crate::{
    clear_screen, command_alias, input, print, println, random_in_range, set_color,
    vga_buffer::Color,
};

use super::CommandMap;

#[derive(Debug, Clone, Copy)]
struct MineField {
    tiles: [[Tile; 9]; 9],
}

impl MineField {
    fn new() -> Self {
        let mut tiles = [[Tile::new(false); 9]; 9];

        let mut mines_placed: u8 = 0;
        while mines_placed < 10 {
            let row = random_in_range(0, 8, 0) as usize;
            let col = random_in_range(0, 8, 0) as usize;

            if !tiles[row][col].is_mine {
                tiles[row][col].is_mine = true;
                mines_placed += 1;

                // notify surrounding tiles of the time
                for i in row.saturating_sub(1)..=(row + 1).min(tiles.len() - 1) {
                    for j in col.saturating_sub(1)..=(col + 1).min(tiles[0].len() - 1) {
                        if i != row || j != col {
                            // skips the currect cell
                            tiles[i][j].adjacent_mines += 1;
                        }
                    }
                }
            }
        }

        Self { tiles }
    }

    /// Check if everything in a row is either discovered or a mine
    fn row_cleared(&self, row: usize) -> Option<bool> {
        Some(
            self.tiles
                .get(row)?
                .iter()
                .all(|t| t.is_discovered || t.is_mine),
        )
    }

    /// Check that the user has discovered all non-mines
    fn check_win(&self) -> bool {
        (0..self.tiles.len()).all(|i| self.row_cleared(i).unwrap())
    }

    /// Flag a tile
    fn flag_tile(&mut self, row: usize, col: usize) {
        if let Some(row_tiles) = self.tiles.get_mut(row) {
            if let Some(tile) = row_tiles.get_mut(col) {
                tile.toggle_flag();
            }
        }
    }

    /// Check a tile. Returns true if the user set off a mine
    fn check_tile(&mut self, row: usize, col: usize) -> Option<bool> {
        // dont set off the mine if it's flagged
        if self.tiles[row][col].is_flagged {
            return Some(false);
        }

        let tiles_copy = self.tiles; // copy tiles for read-only use later

        if let Some(row_tiles) = self.tiles.get_mut(row) {
            if let Some(tile) = row_tiles.get_mut(col) {
                let checked_mine = Some(tile.check_mine());

                if tile.adjacent_mines == 0 {
                    // iterate through the adjacent tiles and start zero-spreading
                    // this will automatically open the adjacent 8 cells if the current cell is 0
                    for i in row.saturating_sub(1)..=(row + 1).min(tiles_copy.len() - 1) {
                        for j in col.saturating_sub(1)..=(col + 1).min(tiles_copy[0].len() - 1) {
                            if i != row || j != col {
                                // skips the currect cell
                                if !tiles_copy[i][j].is_discovered {
                                    self.check_tile(i, j);
                                }
                            }
                        }
                    }
                }
                return checked_mine;
            }
        }
        None
    }

    /// Print the minefield to the screen. We can't use impl DIsplay because of screen writer lock issues.
    fn print_minefield(&self) {
        // write the numbers for the cols
        print!("       ");
        for num in 0..self.tiles[0].len() {
            print!("{}   ", num + 1);
        }
        println!();

        for (row_index, row) in self.tiles.iter().enumerate() {
            print!("{}", char::from_u32((row_index + 65) as u32).unwrap()); // write the row letter
            print!("    |");
            for tile in row.iter() {
                print!(" ");
                tile.print_tile();
                print!(" |");
            }
            println!();
        }

        println!();
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Tile {
    is_mine: bool,
    is_discovered: bool,
    adjacent_mines: u8,
    is_flagged: bool,
}

impl Tile {
    fn new(is_mine: bool) -> Self {
        Self {
            is_mine,
            is_discovered: false,
            adjacent_mines: 0,
            is_flagged: false,
        }
    }

    /// Toggle whether or not the tile is flagged
    fn toggle_flag(&mut self) {
        self.is_flagged = !self.is_flagged;
    }

    /// Set the tile to discovered. Returns true if the user set off a mine.
    fn check_mine(&mut self) -> bool {
        // don't discover the tile if it's flagged
        if self.is_flagged {
            return false;
        }

        // tile isn't flagged
        self.is_discovered = true;

        self.is_mine
    }

    /// Print the tile to the screen. We can't use impl DIsplay because of screen writer lock issues.
    fn print_tile(&self) {
        // List of foreground colors for each number. The first item is useless since we'll never display '0'
        let colors = [
            Color::Red,
            Color::LightBlue,
            Color::Green,
            Color::Red,
            Color::Blue,
            Color::Brown,
            Color::Cyan,
            Color::DarkGrey,
            Color::LightGrey,
        ];
        if self.is_discovered {
            // spot is discovered
            if self.is_mine {
                set_color!(Color::White, Color::Red);
                print!("M");
                set_color!(Color::White, Color::Black);
            } else if self.adjacent_mines != 0 {
                // spot isnt 0, show the number of adjacent mines
                set_color!(colors[self.adjacent_mines as usize], Color::Black);
                print!("{}", self.adjacent_mines);
                set_color!(Color::White, Color::Black);
            } else {
                // spot is 0, show a blank space
                print!(" ");
            }
        } else if self.is_flagged {
            set_color!(Color::Yellow, Color::Black);
            print!("%");
            set_color!(Color::White, Color::Black);
        } else {
            // spot is hidden
            print!("?");
        }
    }
}

fn main(_: &[&str]) {
    let mut user_lost = false;
    let mut field = MineField::new();

    loop {
        clear_screen!();
        println!("======= MINESWEEPER =======");
        println!("Input the coordinates of the tile to open.\nTo flag a cell, prepend the coordinates with the letter f.\n\n");
        // println!("{}", field);
        field.print_minefield();

        if user_lost {
            println!("\nYou lost!");
            break;
        } else if field.check_win() {
            println!("\nYou won!");
            break;
        }

        let mut user_input = input("> ", None);
        // parse user input
        user_input = user_input.replace(' ', "").to_uppercase();
        if user_input.len() >= 2 && user_input.len() <= 3 {
            // extract coordinates
            let mut chars = user_input.chars();
            let user_is_flagging_tile = if user_input.len() == 3 {
                // check for flag flag
                chars.next() == Some('F')
            } else {
                false
            };

            let i = chars.next().unwrap();
            let j = chars.next().unwrap();

            // check which coordinate is the row and convert it from ascii back to
            // the corresponding row index
            let row = if i.is_alphabetic() {
                Some(i as usize - 65)
            } else if j.is_alphabetic() {
                Some(j as usize - 65)
            } else {
                None
            };

            let col = if i.is_numeric() {
                i.to_digit(10)
            } else if j.is_numeric() {
                j.to_digit(10)
            } else {
                None
            };

            if row.is_none() || col.is_none() {
                continue;
            }

            let row = row.unwrap();
            let col = col.unwrap() - 1;

            // row and col are both Some
            if user_is_flagging_tile {
                field.flag_tile(row, col as usize);
            } else {
                user_lost = field.check_tile(row, col as usize).unwrap_or(false);
            }
        }
    }
}

pub fn get_commands() -> CommandMap {
    command_alias! {
        "minesweeper" => (main, "Play minesweeper")
    }
}
