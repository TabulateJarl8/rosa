use crate::{command_alias, println};

use super::CommandMap;

// Print date
pub fn date(args: &[&str]) {
    if args.is_empty() {
        println!("{}", crate::clock::get_current_time());
    } else {
        println!(
            "{}",
            crate::clock::datetime::DateTime::from_unix_timestamp(args[0].parse::<u64>().unwrap())
        );
    }
}

// Print unix timestamp
pub fn udate(_: &[&str]) {
    println!("{}", crate::clock::get_current_time().to_unix_timestamp());
}

// Print uptime
pub fn uptime(_: &[&str]) {
    crate::clock::uptime::print_uptime();
}

// Set timezone
pub fn tz_command(args: &[&str]) {
    crate::clock::tz_command(args);
}

// Set up commands
pub fn get_commands() -> CommandMap {
    command_alias! {
        "date" => (date, "Print the date. If a unix timestamp is provided, it will convert that to a date"),
        "udate" => (udate, "Print the current time as a unix timestamp"),
        "uptime" => (uptime, "Display the system uptime"),
        "tz" => (tz_command, "Set the timezone")
    }
}
