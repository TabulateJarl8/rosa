use crate::{clear_screen, command_alias, println};

use super::CommandMap;

// Print to screen
pub fn echo(args: &[&str]) {
    println!("{}", args.join(" "))
}

// Clear screen
pub fn clear(_: &[&str]) {
    clear_screen!()
}

// Set up commands
pub fn get_commands() -> CommandMap {
    command_alias! {
        "echo" => (echo, "Display a line of text"),
        "clear" => (clear, "Clear the terminal screen")
    }
}
