extern crate alloc;


use hashbrown::HashMap;

use crate::{
    applications::CommandMap,
    command_alias, println,
    sound::play,
};



pub fn ring(args: &[&str]) {
    let mut songs = HashMap::<&str, &str>::new();
    songs.insert("HauntHouse", "HauntHouse: d=4,o=5,b=108: 2a4, 2e, 2d#, 2b4, 2a4, 2c, 2d, 2a#4, 2e., e, 1f4, 1a4, 1d#, 2e., d, 2c., b4, 1a4, 1p, 2a4, 2e, 2d#, 2b4, 2a4, 2c, 2d, 2a#4, 2e., e, 1f4, 1a4, 1d#, 2e., d, 2c., b4, 1a4");
    songs.insert("Mario", "Super Mario - Main Theme:d=4,o=5,b=125:a,8f.,16c,16d,16f,16p,f,16d,16c,16p,16f,16p,16f,16p,8c6,8a.,g,16c,a,8f.,16c,16d,16f,16p,f,16d,16c,16p,16f,16p,16a#,16a,16g,2f,16p,8a.,8f.,8c,8a.,f,16g#,16f,16c,16p,8g#.,2g,8a.,8f.,8c,8a.,f,16g#,16f,8c,2c6");
    songs.insert("MahnaMahna", "MahnaMahna:d=16,o=6,b=125:c#,c.,b5,8a#.5,8f.,4g#,a#,g.,4d#,8p,c#,c.,b5,8a#.5,8f.,g#.,8a#.,4g,8p,c#,c.,b5,8a#.5,8f.,4g#,f,g.,8d#.,f,g.,8d#.,f,8g,8d#.,f,8g,d#,8c,a#5,8d#.,8d#.,4d#,8d#.");
    songs.insert("SmbTheme", "SMBtheme:d=4,o=5,b=100:16e6,16e6,32p,8e6,16c6,8e6,8g6,8p,8g,8p,8c6,16p,8g,16p,8e,16p,8a,8b,16a#,8a,16g.,16e6,16g6,8a6,16f6,8g6,8e6,16c6,16d6,8b,16p,8c6,16p,8g,16p,8e,16p,8a,8b,16a#,8a,16g.,16e6,16g6,8a6,16f6,8g6,8e6,16c6,16d6,8b,8p,16g6,16f#6,16f6,16d#6,16p,16e6,16p,16g#,16a,16c6,16p,16a,16c6,16d6,8p,16g6,16f#6,16f6,16d#6,16p,16e6,16p,16c7,16p,16c7,16c7,p,16g6,16f#6,16f6,16d#6,16p,16e6,16p,16g#,16a,16c6,16p,16a,16c6,16d6,8p,16d#6,8p,16d6,8p,16c6");
    songs.insert("TakeOnMe", "TakeOnMe:d=4,o=4,b=160:8f#5,8f#5,8f#5,8d5,8p,8b,8p,8e5,8p,8e5,8p,8e5,8g#5,8g#5,8a5,8b5,8a5,8a5,8a5,8e5,8p,8d5,8p,8f#5,8p,8f#5,8p,8f#5,8e5,8e5,8f#5,8e5,8f#5,8f#5,8f#5,8d5,8p,8b,8p,8e5,8p,8e5,8p,8e5,8g#5,8g#5,8a5,8b5,8a5,8a5,8a5,8e5,8p,8d5,8p,8f#5,8p,8f#5,8p,8f#5,8e5,8e5");
    songs.insert("FinalCountdown", "FinalCou:d=4,o=5,b=140:16c#6,32b,32p,8c#.6,16p,f#,p.,32d6,32p,32c#6,32p,32d6,16p.,32c#6,16p.,b.,p,32d6,32p,32c#6,32p,d6,f#,p.,32b,32p,32a,32p,32b,16p.,32a,16p.,32g#,16p.,32b,16p.,a.,32c#6,32p,32b,32p,c#6,2f#,p,16p,32d6,32p,32c#6,32p,32d6,16p.,32c#6,16p.,b.,p,32d6,32p,32c#6,32p,d6,f#,p.,32b,32p,32a,32p,32b,16p.,32a,16p.,32g#,16p.,32b,16p.,2a,16p,32g#,32p,32a,32p,b.,16a,16b,8c#6,8b,8a,8g#,f#,d6,1c#6,8p,16c#6,16d6,16c#6,16b,2c#.6,16p
    ");
    songs.insert("NGGYU", "NGGYU:d=4,o=5,b=200:8g,8a,8c6,8a,e6,8p,e6,8p,d6.,p,8p,8g,8a,8c6,8a,d6,8p,d6,8p,c6,8b,a.,8g,8a,8c6,8a,2c6,d6,b,a,g.,8p,g,2d6,2c6.,p,8g,8a,8c6,8a,e6,8p,e6,8p,d6.,p,8p,8g,8a,8c6,8a,2g6,b,c6.,8b,a,8g,8a,8c6,8a,2c6,d6,b,a,g.,8p,g,2d6,2c6.");
    songs.insert("OneMoreTime", "OneMoreT:d=16,o=5,b=125:4e,4e,4e,4e,4e,4e,8p,4d#.,4e,4e,4e,4e,4e,4e,8p,4d#.,4e,4e,4e,4e,4e,4e,8p,4d#.,4f#,4f#,4f#,4f#,4f#,4f#,8f#,4d#.,4e,4e,4e,4e,4e,4e,8p,4d#.,4e,4e,4e,4e,4e,4e,8p,4d#.,1f#,2f#
    ");
    songs.insert("Goodbye", "Porter Rob:d=4,o=5,b=100:1p,2e,2a.,p,6g#,6a,6b,2e,2a.,p,6g#,6a,6b,6d,6a,6a,a.,2p,6g#,6a,6b,2e.,1p,6c#,6d,2e,2a.,p,6g#,6a,6b,2e,2a4,6p,6e,6e,6e,6d,6c#,1a4,2p,6b4,6c#,1a4,1p,2e,2a.,p,6g#,6a,6b,2e,2a.,p,6g#,6a,6b,6d,6a,6a,a.,2p,6g#,6a,6b,2e.,1p,6c#,6d,2e,2a.,p,6g#,6a,6b,2e,2a4,6p,6e,6e,6e,6d,6c#,1a4,2p,6b4,6c#,1a4,1p,2e,2a.,p,6g#,6a,6b,2e,2a.,p,6g#,6a,6b,6d,6a,6a,a.,2p,6g#,6a,6b,2e.,1p,6c#,6d,2e,2a.,p,6g#,6a,6b,2e,2a4,6p,6e,6e,6e,6d,6c#,1a4,1p,8a,p.,8c#6,p.,8a,1p,2e,2a.,p,6g#,6a,6b,2e,2a.,p,6g#,6a,6b,6d,6a,6a,a.,2p,6g#,6a,6b,2e.,1p,6c#,6d,2e,2a.,p,6g#,6a,6b,2e,2a4,6p,6e,6e,6e,6d,6c#,1a4,2p,6b4,6c#,1a4,1p,e,p,2a.,p,8a,38p,8b,27p,e.,8p,2a.,p,g#,9p,9a.,6d,6a,6a,a.,2p,6g#,6a,9b,30p.,2e.,8p.,2c#,p,6c#,6d,2e,2a.,p,6g#,6a,6b,2e,a.4,1e,3e,8a.4");
    songs.insert("Sandstorm", "Sandstor:d=4,o=5,b=125:16e,16e,16e,16e,8e,16e,16e,16e,16e,16e,16e,8e,16a,16a,16a,16a,16a,16a,8a,16g,16g,16g,16g,16g,16g,8g,16d,16d,16e,16e,16e,16e,8e,16e,16e,16e,16e,16e,16e,8e,16a,16a,16e,16e,16e,16e,8e,16e,16e,16e,16e,16e,16e,8e
    ");
    songs.insert("Intel", "Intel:d=4,o=5,b=285:16a,16a#,16b,c#6,2p,c#,f#,c#,g#");

    if args.is_empty() {
        println!("Please specify a song:");
        for song in songs {
            println!("{}", song.0);
        }
        return;
    }

    if songs.contains_key(args[0]) {
        play(songs[args[0]]);
    }
}

pub fn fsharp(_args: &[&str]) {
    crate::sound::fsharp_scale();
}

// Set up commands
pub fn get_commands() -> CommandMap {
    command_alias! {
        "ring" => (ring, "Ring a song")
    }
}
