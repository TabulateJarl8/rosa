use crate::{command_alias, println};
use crate::applications::CommandMap;

pub fn sing(args: &[&str]) {
    if args.is_empty() {
        println!("Please specify a song:");
        println!("hcb");
        println!("fsharp");
        return;
    }

    if args[0] == "hcb" {
        crate::sound::sing();
    } else if args[0] == "fsharp" {
        crate::sound::fsharp_scale();
    }
}

pub fn fsharp(_args: &[&str]) {
    crate::sound::fsharp_scale();
}

// Set up commands
pub fn get_commands() -> CommandMap {
    command_alias! {
        "sing" => (sing, "Sing a song")
    }
}
