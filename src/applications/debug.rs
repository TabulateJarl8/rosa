use crate::{command_alias, println};

use super::CommandMap;

pub fn fault(fault_type: &[&str]) {
    if !fault_type.is_empty() {
        match fault_type[0] {
            "div_zero" => {
                div_zero_fault();
                return;
            }
            "pointer" => {
                pointer_fault();
                return;
            }
            _ => {}
        }
    }
    println!("Error: no fault type specified");
    println!("Available types are: div_zero, pointer");
}

// Crash system by dividing by zero
pub fn div_zero_fault() {
    unsafe {
        core::arch::asm!(
            "
            mov ax, 0
            mov bl, 0
            div bl
        "
        )
    };
}

// Crash system by doing weird pointer things
pub fn pointer_fault() {
    unsafe {
        *(0xdeadbeaf as *mut u64) = 42;
    };
}

// Set up commands
pub fn get_commands() -> CommandMap {
    command_alias! {
        "fault" => (fault, "Crash the operating system in various ways")
    }
}
