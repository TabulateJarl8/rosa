automod::dir!(pub "src/applications");

type CommandMap = hashbrown::HashMap<&'static str, (for<'a, 'b> fn(&'a [&'b str]), &'static str)>;

#[macro_export]
macro_rules! command_alias {
    ($($alias:expr => ($func:path, $desc:expr)),*) => {{
        use hashbrown::HashMap;

        let mut map = HashMap::new();
        $(
            map.insert($alias, ($func as for<'a, 'b> fn(&'a [&'b str]), $desc));
        )*
        map
    }};
}
