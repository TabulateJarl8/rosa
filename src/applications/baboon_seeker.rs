extern crate alloc;

use crate::{command_alias, print, println};
use alloc::{string::String, vec::Vec};

use super::CommandMap;

// Fail game
fn fail() {
    let mut i: u64 = 1;
    while i > 0 {
        print!("Failure");
        for _ in 0..i {
            print!("!");
        }
        println!();
        i += 1;
    }
}

// Stumble and drop an item
fn stumble(inventory: &mut Vec<String>, bamboo: &mut u32) {
    let mut dropped_type = crate::random_in_range(0, 1, 0);
    if *bamboo == 0 {
        dropped_type = 0;
    }

    if dropped_type == 1 {
        let dropped_item =
            inventory.remove(crate::random_in_range(0, (inventory.len() - 1) as u32, 0) as usize);
        println!("You stumbled and dropped a {}", dropped_item);
        if dropped_item == "Baboon Seeking Backpack" {
            fail();
        }
    } else {
        let bamboos = crate::random_in_range(1, *bamboo, 1);
        println!("You stumbled and dropped {} bamboo", bamboos);
        *bamboo -= bamboos;
    }
}

// Show inventory to user
fn show_inv(inventory: &Vec<String>, bamboo: &u32) {
    print!("YOU HAS {} BAMBOO", bamboo);
    for item in inventory {
        print!(" AND YOU HAS A {}", item);
    }
    println!();
}

// Seek for baboons or bamboo
fn seek(inventory: &mut Vec<String>, bamboo: &mut u32) {
    if *bamboo > 0 {
        *bamboo -= 1;
    } else {
        println!("YOU HAS NO BAMBOO!");
    }

    let seeked = crate::random_in_range(0, 100, 100);
    if seeked < 1 {
        println!("You soke a Bamboon");
        stumble(inventory, bamboo);
    } else if seeked < 10 {
        if *bamboo > 0 {
            println!("You soke a Baboon");
            inventory.push(String::from("Baboon"));
        } else {
            println!("You soke a Baboon but it ran away because you are out of bamboo");
        }
    } else if seeked < 50 {
        println!("You soke a Bamboo");
        *bamboo += 2;
    } else {
        println!("You soke a nothing");
    }
}

// Main game function
pub fn main(_: &[&str]) {
    println!("WELCOME TO BABOON SEEKER XTREME ROSA");
    let mut bamboo = crate::random_in_range(5, 20, 0);
    let mut inventory = Vec::<String>::new();
    inventory.push(String::from("Baboon Seeking Backpack"));

    println!("You are the mighty baboon seeker. You have set off into the rosa with your baboon seeking backpack holding {} bamboo to use for seeking baboons.", bamboo);

    loop {
        let cmd = crate::input(": ", None);

        match cmd.as_str() {
            "seek" => seek(&mut inventory, &mut bamboo),
            "WHAT DOES I HAS" => show_inv(&inventory, &bamboo),
            "exit" => break,
            _ => stumble(&mut inventory, &mut bamboo),
        }
    }
}

// Set up commands
pub fn get_commands() -> CommandMap {
    command_alias! {
        "baboonseeker" => (main, "Play the Baboon Seeker game"),
        "bs" => (main, "Play the Baboon Seeker game")
    }
}
