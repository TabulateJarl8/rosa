use core::ops::BitOr;

/// Represents the value to disable Non-Maskable Interrupts (NMI).
pub const NMI_DISABLED: u8 = 0;

/// Represents various RTC registers.
#[derive(Debug, Copy, Clone)]
pub enum Register {
    /// Represents the seconds range from 0 to 59.
    Seconds = 0x00,
    /// Represents the minutes range from 0 to 59.
    Minutes = 0x02,
    /// Represents the hours in 24-hour mode (0–23), or in 12-hour mode (1–12) with the highest bit set if PM.
    Hours = 0x04,
    /// Represents the days of the week, where Sunday is 1 and Saturday is 7.
    Weekday = 0x06,
    /// Represents the day of the month in the range 1 to 31.
    DayOfMonth = 0x07,
    /// Represents the month of the year in the range 1 to 12.
    Month = 0x08,
    /// Represents the year in the range 0 to 99.
    Year = 0x09,
    /// Represents the century in the range 19 to 20 (approximate).
    Century = 0x32,
    /// Status register B
    ///
    /// There are 4 possible formats for the date/time RTC bytes:
    ///  - Binary or BCD Mode
    ///  - Hours in 12-hour format or 24-hour format
    ///
    ///  - Status Register B, Bit 1 (value = 2): Enables 24-hour format if set
    ///  - Status Register B, Bit 2 (value = 4): Enables Binary mode if set
    StatusB = 0x0B,
}

/// Implements the bitwise OR operation for combining a `u8` value with a `Register`.
impl BitOr<Register> for u8 {
    type Output = u8;

    fn bitor(self, rhs: Register) -> Self::Output {
        self | rhs as u8
    }
}

/// Represents the days of the week as strings.
///
/// This array contains string representations of the days of the week.
/// The index corresponds to the numeric representation of each day, where Sunday is 0 and Saturday is 6.
/// For example, `DAYS_OF_WEEK[0]` is "Sun" and `DAYS_OF_WEEK[6]` is "Sat".
pub const DAYS_OF_WEEK: [&str; 7] = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

/// Represents the three-letter abbreviations for each month of the year.
///
/// This array contains string representations of the three-letter abbreviations for each month.
/// The index corresponds to the numeric representation of each month, where January is 0 and December is 11.
/// For example, `MONTHS_ABBREVIATIONS[0]` is "Jan" and `MONTHS_ABBREVIATIONS[11]` is "Dec".
pub const MONTHS_ABBREVIATIONS: [&str; 12] = [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
];
