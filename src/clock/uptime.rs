use crate::println;
use lazy_static::lazy_static;

lazy_static! {
    static ref START_TIME: crate::clock::datetime::DateTime = crate::clock::get_current_time();
}

// Get uptime as number of seconds
pub fn get_uptime() -> u64 {
    (crate::clock::get_current_time().to_unix_timestamp() as i64
        - (START_TIME.to_unix_timestamp() as i64 + 3600 * (*crate::clock::TIME_ZONE.lock())))
        as u64
}

// Print uptime to screen in the format used on unix
pub fn print_uptime() {
    // Get uptime
    let uptime = get_uptime();

    // Get current time
    let date = crate::clock::get_current_time();

    // Calculate days, hours, minutes from uptime seconds
    let days = uptime / 86400;
    let hours = (uptime % 86400) / 3600;
    let minutes = (uptime % 3600) / 60;

    // Print to screen
    println!(
        "{:0>2}:{:0>2}:{:0>2} up {} days {:0>2}:{:0>2}",
        date.hours, date.minutes, date.seconds, days, hours, minutes
    );
}
