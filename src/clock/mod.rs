pub mod consts;
pub mod datetime;
pub mod uptime;
pub mod utils;

extern crate alloc;

// Stores the time zone as the difference from GMT in hours
pub static TIME_ZONE: spin::Mutex<i64> = spin::Mutex::new(0);

use self::{
    consts::Register,
    datetime::DateTime,
    utils::{bcd_to_decimal, read_rtc_register},
};
use crate::println;

/// Retrieves the current time as a `DateTime` object from the Real-Time Clock (RTC).
///
/// This function reads the individual components of the current time (seconds, minutes, hours, etc.)
/// from the RTC registers and formats them into a `DateTime` object.
///
/// # Returns
///
/// Returns a `DateTime` object representing the current time.
pub fn get_current_time() -> DateTime {
    // read all of the components from RTC
    let mut seconds = read_rtc_register(Register::Seconds);
    let mut minutes = read_rtc_register(Register::Minutes);
    let mut hours = read_rtc_register(Register::Hours);
    let mut weekday = read_rtc_register(Register::Weekday);
    let mut day = read_rtc_register(Register::DayOfMonth);
    let mut month = read_rtc_register(Register::Month);
    let mut year = read_rtc_register(Register::Year);
    let mut century = read_rtc_register(Register::Century);

    // get register b for format testing
    let reg_b = read_rtc_register(Register::StatusB);

    // Format is in BCD, not binary
    if reg_b & 0x04 == 0 {
        seconds = bcd_to_decimal(seconds);
        minutes = bcd_to_decimal(minutes);
        // hours is different to account for AM/PM
        hours = ((hours & 0x0F) + (((hours & 0x70) / 16) * 10)) | (hours & 0x80);
        weekday = bcd_to_decimal(weekday);
        day = bcd_to_decimal(day);
        month = bcd_to_decimal(month);
        year = bcd_to_decimal(year);
        if century != 0 {
            century = bcd_to_decimal(century);
        } else {
            // assume we're in the 2000s
            century = 20;
        }
    }

    // convert from 12 hours format to 12 hour foramt if nessecary
    if reg_b & 0x02 == 0 && hours & 0x80 != 0 {
        hours = ((hours & 0x7F) + 12) % 24;
    }

    let mut big_year = year as u32;
    let century = century as u32;
    // add century to year
    big_year += century * 100;

    // Time zone is negative, subtract
    if *TIME_ZONE.lock() < 0 {
        DateTime {
            seconds,
            minutes,
            hours,
            weekday,
            day,
            month,
            year: big_year,
        } - DateTime::from_unix_timestamp(3600 * (*TIME_ZONE.lock()).unsigned_abs())

    // Time zone is positive, add
    } else {
        DateTime {
            seconds,
            minutes,
            hours,
            weekday,
            day,
            month,
            year: big_year,
        } + DateTime::from_unix_timestamp(3600 * (*TIME_ZONE.lock()).unsigned_abs())
    }
}

// Set the time zone
pub fn tz_command(args: &[&str]) {
    if !args.is_empty() {
        // Try to convert the argument into an integer
        let requested_zone = args[0].parse::<i64>();
        // Check if the conversion was successful
        match requested_zone {
            // Conversion was successful, set time zone
            Ok(zone) => {
                let mut guard = TIME_ZONE.lock();
                *guard = zone;
                println!("Timezone set")
            }
            // User provided something other than an integer, error
            _ => {
                println!("Please provide the difference between your time zone and GMT")
            }
        }
    // No argument provided, error
    } else {
        println!("Please provide the difference between your time zone and GMT");
    }
}
