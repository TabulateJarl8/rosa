use x86_64::instructions::port::Port;

use super::consts::{Register, NMI_DISABLED};

/// Reads a value from an RTC register.
///
/// # Arguments
///
/// * `register` - The RTC register to read from (0x00 to 0x7F).
///
/// # Returns
///
/// Returns the value read from the specified RTC register.
pub fn read_rtc_register(register: Register) -> u8 {
    unsafe {
        Port::new(0x70).write((NMI_DISABLED << 7) | register);
        Port::new(0x71).read()
    }
}

/// Checks if a year is a leap year.
///
/// # Arguments
///
/// * `year` - The year to check for leapness.
///
/// # Returns
///
/// Returns `true` if the year is a leap year, otherwise `false`.
pub fn is_leap_year(year: u32) -> bool {
    (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)
}

/// Converts Binary-Coded Decimal (BCD) to Decimal.
///
/// # Arguments
///
/// * `bcd` - The Binary-Coded Decimal value to convert.
///
/// # Returns
///
/// Returns the decimal equivalent of the BCD input.
pub fn bcd_to_decimal(bcd: u8) -> u8 {
    ((bcd & 0xF0) >> 1) + ((bcd & 0xF0) >> 3) + (bcd & 0xf)
}

/// Returns the number of days in a specific month of a given year.
///
/// # Arguments
///
/// * `year` - The year in which the month is located.
/// * `month` - The month for which to determine the number of days.
///
/// # Panics
///
/// Panics if an invalid month value is provided.
///
/// # Returns
///
/// Returns the number of days in the specified month.
pub fn days_in_month(year: u32, month: u8) -> u8 {
    match month {
        1 | 3 | 5 | 7 | 8 | 10 | 12 => 31,
        4 | 6 | 9 | 11 => 30,
        2 => {
            if is_leap_year(year) {
                29
            } else {
                28
            }
        }
        _ => panic!("Invalid month"),
    }
}
