use core::{
    fmt::{self, Display},
    ops::{Add, Sub},
};

use crate::clock::utils::{days_in_month, is_leap_year};

use super::consts::{DAYS_OF_WEEK, MONTHS_ABBREVIATIONS};

/// Represents a datetime object containing information about a timestamp.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct DateTime {
    /// Seconds, ranging from 0 to 59.
    pub seconds: u8,
    /// Minutes, ranging from 0 to 59.
    pub minutes: u8,
    /// Hours, ranging from 0 to 23.
    pub hours: u8,
    /// Weekday, ranging from 1 to 7, where Sunday is 1.
    pub weekday: u8,
    /// Day of the month, ranging from 1 to 31.
    pub day: u8,
    /// Month, ranging from 1 to 12.
    pub month: u8,
    /// Year.
    pub year: u32,
}

impl Display for DateTime {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // Get time zone
        let time_zone = *crate::clock::TIME_ZONE.lock();

        // Decide if it should be displayed with a plus or minus
        let plus_minus = if time_zone > 0 { "+" } else { "-" };

        f.write_fmt(format_args!(
            "{} {} {: >2} {:0>2}:{:0>2}:{:0>2} GMT{}{} {}",
            DAYS_OF_WEEK[(self.weekday - 1) as usize],
            MONTHS_ABBREVIATIONS[(self.month - 1) as usize],
            self.day,
            self.hours,
            self.minutes,
            self.seconds,
            plus_minus,
            time_zone.abs(),
            self.year
        ))
    }
}

impl DateTime {
    /// Converts this datetime object to a Unix timestamp.
    pub fn to_unix_timestamp(&self) -> u64 {
        // Constants for time calculation
        const SECONDS_IN_MINUTE: u64 = 60;
        const SECONDS_IN_HOUR: u64 = 60 * SECONDS_IN_MINUTE;
        const SECONDS_IN_DAY: u64 = 24 * SECONDS_IN_HOUR;

        let mut total_seconds: u64 = 0;

        // Calculate seconds from years
        for year in 1970..self.year {
            total_seconds += if is_leap_year(year) { 366 } else { 365 } * SECONDS_IN_DAY;
        }

        // Calculate seconds from months
        for month in 1..self.month {
            total_seconds += days_in_month(self.year, month) as u64 * SECONDS_IN_DAY;
        }

        // Calculate seconds from days, hours, minutes, and seconds
        total_seconds += (self.day as u64 - 1) * SECONDS_IN_DAY;
        total_seconds += self.hours as u64 * SECONDS_IN_HOUR;
        total_seconds += self.minutes as u64 * SECONDS_IN_MINUTE;
        total_seconds += self.seconds as u64;

        total_seconds
    }

    /// Creates a datetime object from a Unix timestamp.
    pub fn from_unix_timestamp(timestamp: u64) -> DateTime {
        let mut remaining_seconds = timestamp;

        // Calculate years
        let mut year = 1970;
        let mut days_in_year = if is_leap_year(year) { 366 } else { 365 };
        while remaining_seconds >= days_in_year * 24 * 60 * 60 {
            remaining_seconds -= days_in_year * 24 * 60 * 60;
            year += 1;
            days_in_year = if is_leap_year(year) { 366 } else { 365 };
        }

        // Calculate months
        let mut month = 1;
        let mut days_in_current_month = days_in_month(year, month) as u64;
        while remaining_seconds >= days_in_current_month * 24 * 60 * 60 {
            remaining_seconds -= days_in_current_month * 24 * 60 * 60;
            month += 1;
            days_in_current_month = days_in_month(year, month) as u64;
        }

        // Calculate days
        let days = (remaining_seconds / (24 * 60 * 60)) as u32 + 1;

        // Calculate hours, minutes, and seconds
        let hours = ((remaining_seconds % (24 * 60 * 60)) / (60 * 60)) as u8;
        let minutes = ((remaining_seconds % (60 * 60)) / 60) as u8;
        let seconds = (remaining_seconds % 60) as u8;

        // Calculate weekday using Zeller's Congruence algorithm
        let mut y = year;
        let mut m = month;
        if month < 3 {
            y -= 1;
            m += 12;
        }
        let k = y % 100;
        let j = y / 100;

        let day_of_week = (days + ((13 * (m as u32 + 1)) / 5) + k + (k / 4) + (j / 4) - 2 * j) % 7;

        let weekday = if day_of_week == 0 {
            // 0 is saturday with Zeller's Congruence, change it to 6
            day_of_week + 6
        } else {
            // else, we just decrement by 1
            day_of_week - 1
        } + 1; // add one since our weekday number is 1-7, not 0-6

        DateTime {
            seconds,
            minutes,
            hours,
            weekday: weekday as u8,
            day: days as u8,
            month,
            year,
        }
    }
}

impl From<u64> for DateTime {
    /// Converts a Unix timestamp to a `DateTime` object.
    fn from(value: u64) -> Self {
        Self::from_unix_timestamp(value)
    }
}

impl Add<DateTime> for DateTime {
    type Output = DateTime;

    /// Adds two `DateTime` objects and returns a new `DateTime`.
    fn add(self, rhs: DateTime) -> Self::Output {
        (self.to_unix_timestamp() + rhs.to_unix_timestamp()).into()
    }
}

impl Sub<DateTime> for DateTime {
    type Output = DateTime;

    /// Subtracts two `DateTime` objects and returns a new `DateTime`.
    fn sub(self, rhs: DateTime) -> Self::Output {
        (self.to_unix_timestamp() - rhs.to_unix_timestamp()).into()
    }
}

/// Sleep for some amount of time. This isn't exactly ticks, as the
/// instructions generated is not really defined. This will also change
/// depending on the CPU its running on.
pub fn sleep(ticks: u64) {
    let mut elapsed_ticks: u64 = 0;
    while elapsed_ticks < ticks {
        elapsed_ticks += 1;
    }
}
