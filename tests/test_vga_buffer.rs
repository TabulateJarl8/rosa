#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(rosa::testing::test_runner)]
#![reexport_test_harness_main = "test_main"]

use rosa::{
    cursor_control, println,
    vga_buffer::{Buffer, Color, ColorCode, Writer, BUFFER_HEIGHT, WRITER},
};

#[no_mangle]
pub extern "C" fn _start() -> ! {
    test_main();

    loop {}
}

#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    rosa::testing::test_panic_handler(info);
}

// Helper function to initialize a Writer for testing
fn init_test_writer() -> Writer {
    Writer {
        column_position: 0,
        color_code: ColorCode::new(Color::Pink, Color::Black),
        buffer: unsafe { &mut *(0xb8000 as *mut Buffer) },
    }
}

#[test_case]
fn test_write_byte() {
    let mut writer = init_test_writer();
    writer.write_byte(b'A');

    let screen_char = writer.buffer.chars[BUFFER_HEIGHT - 1][0].read();
    assert_eq!(screen_char.ascii_character, b'A');
}

#[test_case]
fn test_write_string() {
    let mut writer = init_test_writer();
    writer.write_string("Hello");

    let screen_char = writer.buffer.chars[BUFFER_HEIGHT - 1][0].read();
    assert_eq!(screen_char.ascii_character, b'H');
}

#[test_case]
fn test_backspace() {
    let mut writer = init_test_writer();
    writer.write_string("Hello");
    writer.backspace();

    let screen_char = writer.buffer.chars[BUFFER_HEIGHT - 1][4].read();
    assert_eq!(screen_char.ascii_character, b' ');

    let cursor_position = cursor_control::get_cursor_position();
    assert_eq!(cursor_position.0, 4); // Check cursor position after backspace
}

#[test_case]
fn test_clear_screen() {
    let mut writer = init_test_writer();
    writer.write_string("Hello");
    writer.clear_screen();

    let screen_char = writer.buffer.chars[BUFFER_HEIGHT - 1][0].read();
    assert_eq!(screen_char.ascii_character, b' ');

    let cursor_position = cursor_control::get_cursor_position();
    assert_eq!(cursor_position, (5, BUFFER_HEIGHT as u16 - 1)); // Check cursor position after clearing
}

#[test_case]
fn test_set_color() {
    let mut writer = init_test_writer();
    writer.set_color(Color::Red, Color::Green);

    let screen_char = writer.buffer.chars[BUFFER_HEIGHT - 1][0].read();
    assert_eq!(
        screen_char.color_code,
        ColorCode::new(Color::Red, Color::Green)
    );
}

#[test_case]
fn test_new_line() {
    let mut writer = init_test_writer();
    writer.write_string("Hello\n");

    let screen_char = writer.buffer.chars[BUFFER_HEIGHT - 2][0].read();
    assert_eq!(screen_char.ascii_character, b'H');
}

#[test_case]
fn test_clear_screen() {
    let mut writer = init_test_writer();
    writer.write_string("Hello\nthis\nis\na\ntest");
    writer.clear_screen();

    let screen_char = writer.buffer.chars[BUFFER_HEIGHT - 1][0].read();
    assert_eq!(screen_char.ascii_character, b' ');
}

#[test_case]
fn test_println_simple() {
    println!("test_println_simple output");
}

#[test_case]
fn test_println_many() {
    for _ in 0..200 {
        println!("test_println_many output");
    }
}

#[test_case]
fn test_println_output() {
    let s = "Some test string that fits on a single line";
    println!("{}", s);
    for (i, c) in s.chars().enumerate() {
        let screen_char = WRITER.lock().buffer.chars[BUFFER_HEIGHT - 2][i].read();
        assert_eq!(char::from(screen_char.ascii_character), c);
    }
}
