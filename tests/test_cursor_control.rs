#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(rosa::testing::test_runner)]
#![reexport_test_harness_main = "test_main"]

use rosa::cursor_control::{disable_cursor, enable_cursor, get_cursor_position, update_cursor};
use x86_64::instructions::port::Port;

#[no_mangle]
pub extern "C" fn _start() -> ! {
    test_main();

    loop {}
}

#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    rosa::testing::test_panic_handler(info);
}

#[test_case]
fn test_enable_and_disable_cursor() {
    // Ensure cursor is initially disabled
    disable_cursor();

    // Enable the cursor with specific start and end positions
    enable_cursor(10, 15);

    // Check if the cursor is enabled by reading from the cursor start register (0x3D5) and comparing with the expected value
    let cursor_start_status = unsafe { Port::<u8>::new(0x3D5).read() };
    assert_eq!(cursor_start_status, 15);

    // Disable the cursor
    disable_cursor();

    // Check if the cursor is disabled by reading from the cursor start register (0x3D5) and comparing with the expected value
    let cursor_start_status = unsafe { Port::<u8>::new(0x3D5).read() };
    assert_eq!(cursor_start_status, 0x20);
}

#[test_case]
fn test_update_cursor() {
    enable_cursor(0, 15);
    // Update the cursor position
    update_cursor(5, 8);

    // Check if the cursor position is updated by reading from the cursor location registers (0x3D5) and comparing with the expected values
    let cursor_position = get_cursor_position();

    assert_eq!(cursor_position.0, 5);
    assert_eq!(cursor_position.1, 8);
}

#[test_case]
fn test_get_cursor_position() {
    // Get the cursor position
    let (x, y) = get_cursor_position();

    // Check if the cursor position matches the expected values
    assert_eq!(x, 0);
    assert_eq!(y, 0);
}
