#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(rosa::testing::test_should_panic_runner)]
#![reexport_test_harness_main = "test_main"]

#[no_mangle]
pub extern "C" fn _start() -> ! {
    test_main();

    loop {}
}

#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    rosa::testing::test_should_panic_handler(info);
}

#[test_case]
fn test_panic_on_ptr_error() {
    unsafe {
        *(0xdeadbeaf as *mut u64) = 42;
    };
}
